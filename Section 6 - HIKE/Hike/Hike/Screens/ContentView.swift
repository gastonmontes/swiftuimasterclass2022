//
//  ContentView.swift
//  Hike
//
//  Created by Gastón Montes on 15/11/2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        CardView()
    }
}

#Preview {
    ContentView()
}
