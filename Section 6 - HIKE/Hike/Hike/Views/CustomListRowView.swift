//
//  CustomListRowView.swift
//  Hike
//
//  Created by Gastón Montes on 21/11/2023.
//

import SwiftUI

struct CustomListRowView: View {
    // MARK: - Properties.
    @State var rowLabel: String
    @State var rowIcon: String
    @State var rowContent: String? = nil
    @State var rowTintColor: Color
    @State var rowLinkLabel: String? = nil
    @State var rowLinkDestination: String? = nil
    
    // MARK: - Body.
    var body: some View {
        LabeledContent {
            if let rowContent = self.rowContent {
                Text(rowContent)
                    .foregroundStyle(Color.primary)
                    .fontWeight(.heavy)
            } else if let rowLink = self.rowLinkLabel, 
                        let rowDestination = self.rowLinkDestination {
                Link(rowLink, destination: URL(string: rowDestination)!)
                    .foregroundStyle(Color.pink)
                    .fontWeight(.heavy)
            } else {
                /*@START_MENU_TOKEN@*/EmptyView()/*@END_MENU_TOKEN@*/
            }
        } label: {
            HStack {
                ZStack {
                    RoundedRectangle(cornerRadius: 8)
                        .frame(width: 30, height: 30)
                        .foregroundStyle(self.rowTintColor)
                    
                    Image(systemName: self.rowIcon)
                        .foregroundStyle(Color.white)
                        .fontWeight(.semibold)
                }
                
                Text(self.rowLabel)
            }
        }
    }
}

// MARK: - Preview.
#Preview {
    List {
        CustomListRowView(rowLabel: "Designer",
                          rowIcon: "paintpalette",
                          rowContent: "John Doe",
                          rowTintColor: .pink)
        
        CustomListRowView(rowLabel: "Website",
                          rowIcon: "globe",
                          rowTintColor: .pink,
                          rowLinkLabel: "Credo Academy",
                          rowLinkDestination: "https://credo.academy")
    }
}
