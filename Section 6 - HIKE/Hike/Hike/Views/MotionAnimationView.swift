//
//  MotionAnimationView.swift
//  Hike
//
//  Created by Gastón Montes on 17/11/2023.
//

import SwiftUI

struct MotionAnimationView: View {
    // MARK: - Properties.
    @State private var randomCircle: Int = Int.random(in: 6...12)
    @State private var isAnimating: Bool = false
    
    // MARK: - Functions.
    private func randomCoordinate() -> CGPoint {
        return CGPoint(x: CGFloat.random(in: 0...256), y: CGFloat.random(in: 0...256))
    }
    
    private func randomSize() -> CGFloat {
        return CGFloat.random(in: 4...80)
    }
    
    private func randomScale() -> CGFloat {
        return CGFloat(Double.random(in: 0.1...2.0))
    }
    
    private func randomSpeed() -> Double {
        return Double.random(in: 0.05...1.0)
    }
    
    private func randomDelay() -> Double {
        return Double.random(in: 0...2)
    }
    
    // MARK: - Body.
    var body: some View {
        ZStack {
            ForEach(0...self.randomCircle, id: \.self) { item in
                Circle()
                    .foregroundStyle(Color.white)
                    .opacity(0.25)
                    .frame(width: self.randomSize())
                    .position(self.randomCoordinate())
                    .scaleEffect(self.isAnimating ? randomScale() : 1)
                    .onAppear(perform: {
                        withAnimation(
                            .interpolatingSpring(
                                stiffness: 0.25,
                                damping: 0.25
                            )
                            .repeatForever()
                            .speed(self.randomSpeed())
                            .delay(self.randomDelay())
                        ) {
                            self.isAnimating = true
                        }
                    })
            }
        } // : ZStack.
        .frame(width: 256, height: 256)
        .mask(Circle())
        .drawingGroup()
    }
}
    
#Preview {
    MotionAnimationView()
        .background(
            Circle()
                .fill(.teal)
        )
}
