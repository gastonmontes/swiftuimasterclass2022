//
//  HikeApp.swift
//  Hike
//
//  Created by Gastón Montes on 15/11/2023.
//

import SwiftUI

@main
struct HikeApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
