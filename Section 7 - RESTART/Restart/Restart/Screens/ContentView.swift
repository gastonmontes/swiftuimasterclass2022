//
//  ContentView.swift
//  Restart
//
//  Created by Gastón Montes on 18/02/2022.
//

import SwiftUI

struct ContentView: View {
    @AppStorage("onboarding") var isOnboardingViewActive: Bool = true
    
    var body: some View {
        ZStack {
            if self.isOnboardingViewActive {
                OnboardingScreen()
            } else {
                HomeScreen()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
