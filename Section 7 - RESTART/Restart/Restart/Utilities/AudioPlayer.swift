//
//  AudioPlayer.swift
//  Restart
//
//  Created by Gastón Montes on 23/02/2022.
//

import Foundation
import AVFoundation

class AudioPlayer {
    static let shared = AudioPlayer()
    
    private var audioPlayer: AVAudioPlayer?
    
    private init() {}
    
    func play(sound: String, type: String) {
        if let path = Bundle.main.path(forResource: sound, ofType: type) {
            do {
                self.audioPlayer = try AVAudioPlayer(contentsOf: URL(fileURLWithPath: path))
                self.audioPlayer?.play()
            } catch {
                print("Could not play the audio file.")
            }
        }
    }
}
