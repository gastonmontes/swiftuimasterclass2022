//
//  RestartApp.swift
//  Restart
//
//  Created by Gastón Montes on 18/02/2022.
//

import SwiftUI

@main
struct RestartApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
