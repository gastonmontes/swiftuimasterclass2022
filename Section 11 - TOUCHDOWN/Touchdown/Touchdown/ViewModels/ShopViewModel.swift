//
//  ShopViewModel.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/24/24.
//

import Foundation

class ShopViewModel: ObservableObject {
    // MARK: - Properties.
    private(set) var productList = ProductModel.productList
    
    // MARK: - Published.
    @Published private(set) var showingProduct: Bool = false
    @Published private(set) var selectedProduct: ProductModel? = nil
    
    // MARK: - Functions.
    func userDidSelected(product: ProductModel) {
        self.showingProduct = true
        self.selectedProduct = product
    }
    
    func userDidCloseDetails() {
        self.showingProduct = false
        self.selectedProduct = nil
    }
}
