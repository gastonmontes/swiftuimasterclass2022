//
//  TouchdownApp.swift
//  Touchdown
//
//  Created by Gastón Montes on 27/06/2024.
//

import SwiftUI
import SwiftData

@main
struct TouchdownApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
