//
//  GridSectionView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct GridSectionView: View {
    // MARK: - Properties.
    @State var rotateClockwise: Bool
    
    // MARK: - Body.
    var body: some View {
        VStack(spacing: 0) {
            Spacer()
            
            Text("Categories".uppercased())
                .font(.footnote)
                .fontWeight(.bold)
                .foregroundStyle(Color.tdGridSectionColor)
                .rotationEffect(.degrees(self.rotateClockwise ? 90 : -90))
            
            Spacer()
        } // : VStack.
        .background(Color.tdColorGray.clipShape(RoundedRectangle(cornerRadius: 12)))
        .frame(width: 84)
    }
}

// MARK: - Preview.
#Preview {
    GridSectionView(rotateClockwise: true)
        .previewLayout(.fixed(width: 120, height: 240))
        .padding()
        .background(Color.tdColorBackground)
}
