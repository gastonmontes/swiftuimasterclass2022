//
//  TitleView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct TitleView: View {
    // MARK: - Properties.
    var title: String
    
    // MARK: - Body.
    var body: some View {
        HStack {
            Text(self.title)
                .font(.largeTitle)
                .fontWeight(.heavy)
            
            Spacer()
        } // : HStack.
        .padding(.horizontal)
        .padding(.top, 16)
        .padding(.bottom, 8)
    }
}

// MARK: - Preview.
#Preview {
    TitleView(title: "Helmet")
        .previewLayout(.sizeThatFits)
        .background(Color.tdColorBackground)
}
