//
//  FooterView.swift
//  Touchdown
//
//  Created by Gastón Montes on 28/06/2024.
//

import SwiftUI

struct FooterView: View {
    // MARK - Body.
    var body: some View {
        VStack(alignment: .center, spacing: 10, content: {
            Text("We offer the most cutting edge, confortable, lightweight and durable football hemets in the market at effordable prices.")
                .foregroundStyle(Color.gray)
                .multilineTextAlignment(.center)
                .layoutPriority(2)
            
            Image("logo-lineal")
                .renderingMode(.template)
                .foregroundStyle(Color.gray)
                .layoutPriority(0)
            
            Text("Copyright © Gastón Montes\nAll right reserved")
                .font(.footnote)
                .fontWeight(.bold)
                .foregroundStyle(Color.gray)
                .multilineTextAlignment(.center)
                .layoutPriority(1)
        }) // : VStack.
        .padding()
    }
}

// MARK - Previews.
#Preview {
    FooterView()
        .previewLayout(.sizeThatFits)
        .background(Color.tdColorBackground)
}
