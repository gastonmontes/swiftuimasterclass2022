//
//  FeatureTabView.swift
//  Touchdown
//
//  Created by Gastón Montes on 28/06/2024.
//

import SwiftUI

struct FeatureTabView: View {
    // MARK - Properties.
    let players: [PlayerModel]
    
    // MARK - Body.
    var body: some View {
        TabView {
            ForEach(players) { player in
                FeaturedItemView(player: player)
                    .padding(.top, 10)
                    .padding(.horizontal, 15)
            }
        } //: TabView.
        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .always))
    }
}

// MARK - Preview.
#Preview {
    FeatureTabView(players: PlayerModel.playerList)
        .background(Color.gray)
}
