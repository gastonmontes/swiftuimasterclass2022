//
//  FeaturedItemView.swift
//  Touchdown
//
//  Created by Gastón Montes on 28/06/2024.
//

import SwiftUI

struct FeaturedItemView: View {
    // MARK - Properties.
    let player: PlayerModel
    
    // MARK - Body.
    var body: some View {
        Image(player.image)
            .resizable()
            .scaledToFit()
            .cornerRadius(12)
    }
}

// MARK - Preview.
#Preview {
    FeaturedItemView(player: PlayerModel.playerList[0])
        .previewLayout(.sizeThatFits)
        .padding()
        .background(Color.tdColorBackground)
}
