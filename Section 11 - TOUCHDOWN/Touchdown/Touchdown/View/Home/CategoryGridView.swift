//
//  CategoryGridView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct CategoryGridView: View {
    // MARK - Properties.
    let categories: [CategoryModel]
    
    // MARK - Body.
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            LazyHGrid(rows: GridLayoutConstants.items,
                      alignment: .center, 
                      spacing: GridLayoutConstants.spacingColumn,
                      pinnedViews: [],
                      content: {
                Section {
                    ForEach(self.categories) { category in
                        CategoryItemView(category: category)
                    }
                } header: {
                    GridSectionView(rotateClockwise: false)
                } footer: {
                    GridSectionView(rotateClockwise: true)
                } // : Section
            }) // : LazyHGrid.
            .frame(height: 140)
            .padding(.horizontal, 16)
            .padding(.vertical, 8)
        } // : ScrollView.
    }
}

// MARK - Preview.
#Preview {
    CategoryGridView(categories: CategoryModel.categoryList)
        .previewLayout(.sizeThatFits)
        .padding()
        .background(Color.tdColorBackground)
}
