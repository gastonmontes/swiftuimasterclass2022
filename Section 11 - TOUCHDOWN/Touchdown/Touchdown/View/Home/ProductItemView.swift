//
//  ProductItemView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct ProductItemView: View {
    // MARK: - Properties.
    let product: ProductModel
    
    // MARK: - Body.
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            // Product photo.
            ZStack {
                Image(self.product.image)
                    .resizable()
                    .scaledToFit()
                    .padding(8)
            } // : ZStack.
            .background(self.product.productColor)
            .clipShape(RoundedRectangle(cornerRadius: 12))
            
            // Product name.
            Text(self.product.name)
                .font(.title3)
                .fontWeight(.black)
            
            // Product price.
            Text("$\(self.product.formattedPrice)")
                .fontWeight(.semibold)
                .foregroundStyle(Color.tdGridItemColor)
        }
    }
}

// MARK: - Preview.
#Preview {
    ProductItemView(product: ProductModel.productList[0])
        .previewLayout(.fixed(width: 200, height: 300))
        .padding()
        .background(Color.tdColorBackground)
}
