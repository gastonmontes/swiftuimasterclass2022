//
//  CategoryItemView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct CategoryItemView: View {
    // MARK: - Properties.
    let category: CategoryModel
    
    // MARK: - Body.
    var body: some View {
        Button(action: {}) {
            HStack(alignment: .center) {
                Image(self.category.image)
                    .renderingMode(.template)
                    .resizable()
                    .scaledToFit()
                    .frame(width: 32, height: 32, alignment: .center)
                    .foregroundStyle(Color.tdGridItemColor)
                
                Text(self.category.name.uppercased())
                    .fontWeight(.light)
                    .foregroundStyle(Color.tdGridItemColor)
                
                Spacer()
            } // : HStack.
            .padding()
            .background(Color.tdGridItemBackgroundColor.clipShape(RoundedRectangle(cornerRadius: 12)))
            .background(
                RoundedRectangle(cornerRadius: 12)
                    .stroke(Color.tdGridItemColor, lineWidth: 1)
            )
        } // : Button.
    }
}

// MARK: - Preview.
#Preview {
    CategoryItemView(category: CategoryModel.categoryList[0])
        .previewLayout(.sizeThatFits)
        .padding()
        .background(Color.tdColorBackground)
}
