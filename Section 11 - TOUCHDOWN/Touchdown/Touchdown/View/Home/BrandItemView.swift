//
//  BrandItemView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct BrandItemView: View {
    // MARK: - Properties.
    let brand: BrandModel
    
    // MARK: - Body.
    var body: some View {
        Image(self.brand.image)
            .resizable()
            .scaledToFit()
            .padding()
            .background(Color.tdGridItemBackgroundColor.clipShape(RoundedRectangle(cornerRadius: 12)))
            .background(
                RoundedRectangle(cornerRadius: 12)
                    .stroke(Color.tdGridItemColor, lineWidth: 1)
            )
    }
}

// MARK: - Preview.
#Preview {
    BrandItemView(brand: BrandModel.brandList[0])
        .previewLayout(.sizeThatFits)
        .padding()
        .background(Color.tdColorBackground)
}
