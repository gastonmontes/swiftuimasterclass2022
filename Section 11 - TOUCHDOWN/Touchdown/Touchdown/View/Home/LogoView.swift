//
//  LogoView.swift
//  Touchdown
//
//  Created by Gastón Montes on 28/06/2024.
//

import SwiftUI

struct LogoView: View {
    // MARK - Body.
    var body: some View {
        HStack {
            Text("Touch".uppercased())
                .font(.title3)
                .fontWeight(.black)
                .foregroundStyle(Color.black)
            
            Image("logo-dark")
                .resizable()
                .scaledToFit()
                .frame(width: 32, height: 32, alignment: .center)
            
            Text("Down".uppercased())
                .font(.title3)
                .fontWeight(.black)
                .foregroundStyle(Color.black)
        }
    }
}

// MARK - Preview.
#Preview {
    LogoView()
        .previewLayout(.sizeThatFits)
        .padding(/*@START_MENU_TOKEN@*/EdgeInsets()/*@END_MENU_TOKEN@*/)
}
