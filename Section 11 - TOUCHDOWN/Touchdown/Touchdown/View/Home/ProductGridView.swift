//
//  ProductGridView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct ProductGridView: View {
    // MARK: - Observed Properties.
    @ObservedObject var shopViewModel: ShopViewModel
    
    // MARK: - Body.
    var body: some View {
        VStack {
            TitleView(title: "Helmets")
            
            LazyVGrid(columns: GridLayoutConstants.items,
                      spacing: 16,
                      content: {
                ForEach(self.shopViewModel.productList) { product in
                    ProductItemView(product: product)
                        .onTapGesture {
                            FeedbackHaptick.shared.feedbackImpacted()
                            
                            withAnimation(.easeOut) {
                                self.shopViewModel.userDidSelected(product: product)
                            }
                        }
                } // : ForEach.
            }) // : LazyVGrid.
            .padding(16)
        } // : VStack.
    }
}

// MARK: - Preview.
#Preview {
    ProductGridView(shopViewModel: ShopViewModel())
        .previewLayout(.sizeThatFits)
}
