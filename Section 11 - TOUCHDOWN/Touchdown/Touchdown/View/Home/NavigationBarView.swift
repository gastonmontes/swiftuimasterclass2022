//
//  NavigationBarView.swift
//  Touchdown
//
//  Created by Gastón Montes on 28/06/2024.
//

import SwiftUI

struct NavigationBarView: View {
    // MARK - Properties.
    @State private var isAnimating = false
    
    // MARK - Body.
    var body: some View {
        HStack {
            Button(action: {}, label: {
                Image(systemName: "magnifyingglass")
                    .font(.title)
                    .foregroundStyle(Color.black)
            }) // : Button.
            
            Spacer()
            
            LogoView()
                .opacity(self.isAnimating ? 1 : 0)
                .offset(x: 0, y: self.isAnimating ? 0 : -25)
                .onAppear(perform: {
                    withAnimation(.easeOut(duration: 0.5)) {
                        self.isAnimating.toggle()
                    }
                })
            
            Spacer()
            
            Button(action: {}, label: {
                ZStack {
                    Image(systemName: "cart")
                        .font(.title)
                    .foregroundStyle(Color.black)
                    
                    Circle()
                        .fill(Color.red)
                        .frame(width: 16, height: 16, alignment: .center)
                        .offset(x: 13, y: -10)
                }
            }) // : Button.
        } // : HStack.
    }
}

// MARK - Previews.
#Preview {
    NavigationBarView()
        .previewLayout(.sizeThatFits)
        .padding()
}
