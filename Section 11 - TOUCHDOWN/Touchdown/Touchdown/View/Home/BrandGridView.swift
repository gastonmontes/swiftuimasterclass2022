//
//  BrandGridView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct BrandGridView: View {
    // MARK: - Properties.
    let brands: [BrandModel]
    
    // MARK: - Body.
    var body: some View {
        VStack {
            TitleView(title: "Brand")
            
            ScrollView(.horizontal, showsIndicators: false) {
                LazyHGrid(rows: GridLayoutConstants.items,
                          spacing: GridLayoutConstants.spacingColumn, content: {
                    ForEach(self.brands) { brand in
                        BrandItemView(brand: brand)
                    }
                }) // : Grid.
                .frame(height: 200)
                .padding(16)
            } // : ScrollView.
        } // : VStack.
    }
}

// MARK: - Preview.
#Preview {
    BrandGridView(brands: BrandModel.brandList)
        .previewLayout(.sizeThatFits)
        .background(Color.tdColorBackground)
}
