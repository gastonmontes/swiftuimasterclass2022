//
//  ProductRatingSizesView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct ProductRatingSizesView: View {
    // MARK: - Properties.
    private let sizes = ["XS", "S", "M", "L", "XL"]
    
    // MARK: - Body.
    var body: some View {
        HStack(alignment: .top, spacing: 4) {
            // Rating.
            VStack(alignment: .leading, spacing: 4) {
                Text("Ratings")
                    .font(.footnote)
                    .fontWeight(.semibold)
                    .foregroundStyle(Color.tdColorGray)
                
                HStack(alignment: .center, spacing: 4) {
                    ForEach(1...5, id: \.self) { item in
                        Button {
                            
                        } label: {
                            Image(systemName: "star.fill")
                                .frame(width: 32, height: 32, alignment: .center)
                                .background(
                                    Color.tdColorGray
                                        .clipShape(RoundedRectangle(cornerRadius: 4))
                                )
                                .foregroundStyle(Color.tdDetailNavBarForegroundColor)
                        }

                    }
                }
            } // : VStack.
            
            Spacer()
            
            // Sizes.
            VStack(alignment: .trailing, spacing: 4) {
                Text("Sizes")
                    .font(.footnote)
                    .fontWeight(.semibold)
                    .foregroundStyle(Color.tdColorGray)
                
                HStack(alignment: .center, spacing: 4) {
                    ForEach(self.sizes, id: \.self) { size in
                        Button {
                            
                        } label: {
                            Text(size)
                                .font(.footnote)
                                .fontWeight(.heavy)
                                .foregroundStyle(Color.tdColorGray)
                                .frame(width: 32, height: 32, alignment: .center)
                                .background(
                                    Color.tdDetailCardBackgroundColor
                                        .clipShape(RoundedRectangle(cornerRadius: 4))
                                )
                                .background(
                                    RoundedRectangle(cornerRadius: 4)
                                        .stroke(Color.tdColorGray, lineWidth: 2)
                                )
                        }

                    }
                }
            }
        } // : HStack.
    }
}

// MARK: - Previews.
#Preview {
    ProductRatingSizesView()
        .previewLayout(.sizeThatFits)
        .padding()
}
