//
//  ProductDetailsView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct ProductDetailsView: View {
    // MARK: - Properties.
    @ObservedObject var shopViewModel: ShopViewModel
    
    // MARK: - Body.
    var body: some View {
        if let product = self.shopViewModel.selectedProduct {
            VStack(alignment: .leading, spacing: 8) {
                // Nav Bar.
                ProductDetailNavigationBar(shopViewModel: self.shopViewModel)
                    .padding(.horizontal)
                
                // Header.
                ProductHeaderView(product: product)
                    .padding(.horizontal)
                
                // Top.
                ProductTopDetailView(product: product)
                    .padding(.horizontal)
                    .zIndex(1)
                
                // Bottom
                ProductBottomDetailView(product: product)
            } // : VStack.
            .zIndex(0)
            .background(
                product.productColor
                    .ignoresSafeArea(.all, edges: .all)
            )
            .ignoresSafeArea(.all, edges: .bottom)
        } else {
            EmptyView()
        }
    }
}

// MARK: - Previews.
#Preview {
    let viewModel = ShopViewModel()
    let selectedItem = ProductModel.productList[2]
    viewModel.userDidSelected(product: selectedItem)
    return ProductDetailsView(shopViewModel: viewModel)
        .previewLayout(.fixed(width: 375, height: 812))
}
