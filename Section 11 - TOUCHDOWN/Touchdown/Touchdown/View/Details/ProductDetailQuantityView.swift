//
//  ProductDetailQuantityView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/24/24.
//

import SwiftUI

struct ProductDetailQuantityView: View {
    // MARK: - Properties.
    @State private var counter: Int = 0
    
    // MARK: - Body.
    var body: some View {
        HStack(alignment: .center, spacing: 8) {
            Button {
                if self.counter > 0 {
                    FeedbackHaptick.shared.feedbackImpacted()
                    self.counter -= 1
                }
            } label: {
                Image(systemName: "minus.circle")
            } // : Button.
            
            Text("\(self.counter)")
                .fontWeight(.semibold)
                .frame(width: 32)
            
            Button {
                FeedbackHaptick.shared.feedbackImpacted()
                self.counter += 1
            } label: {
                Image(systemName: "plus.circle")
            } // : Button.
            
            Spacer()
            
            Button {
                FeedbackHaptick.shared.feedbackImpacted()
            } label: {
                Image(systemName: "heart.circle")
                    .foregroundStyle(Color.tdQuantityDetailFavoriteColor)
            } // : Button.
        } // : HStack.
        .font(.system(.title, design: .rounded))
        .foregroundStyle(Color.tdQuantityDetailForegroundColor)
        .imageScale(.large)
    }
}

// MARK: - Preview.
#Preview {
    ProductDetailQuantityView()
        .previewLayout(.sizeThatFits)
        .padding()
}
