//
//  ProductDetailNavigationBar.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct ProductDetailNavigationBar: View {
    // MARK: - Properties.
    @ObservedObject var shopViewModel: ShopViewModel
    
    // MARK: - Body.
    var body: some View {
        HStack {
            Button {
                withAnimation(.easeIn) {
                    FeedbackHaptick.shared.feedbackImpacted()
                    self.shopViewModel.userDidCloseDetails()
                }
            } label: {
                Image(systemName: "chevron.left")
                    .font(.title)
                    .foregroundStyle(Color.tdDetailNavBarForegroundColor)
            }
            
            Spacer()
            
            Button {
                
            } label: {
                Image(systemName: "cart")
                    .font(.title)
                    .foregroundStyle(Color.tdDetailNavBarForegroundColor)
            }
        } // : HStack.
    }
}

// MARK: - Preview.
#Preview {
    ProductDetailNavigationBar(shopViewModel: ShopViewModel())
        .previewLayout(.sizeThatFits)
        .padding()
        .background(Color.tdDetailBackgroundColor)
}
