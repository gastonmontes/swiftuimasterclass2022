//
//  ProductDetailAddToCartView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/24/24.
//

import SwiftUI

struct ProductDetailAddToCartView: View {
    // MARK: - Properties.
    let product: ProductModel
    
    // MARK: - Body.
    var body: some View {
        Button {
            FeedbackHaptick.shared.feedbackImpacted()
        } label: {
            Spacer()
            Text("Add to cart".uppercased())
                .font(.system(.title2, design: .rounded))
                .fontWeight(.bold)
                .foregroundStyle(Color.tdCartDetailForegroundColor)
            Spacer()
        } // : Button.
        .padding(16)
        .background(self.product.productColor)
        .clipShape(Capsule())
    }
}

// MARK: - Preview.
#Preview {
    ProductDetailAddToCartView(product: ProductModel.productList[0])
        .previewLayout(.sizeThatFits)
        .padding()
}
