//
//  ProductTopDetailView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct ProductTopDetailView: View {
    // MARK: - Properties.
    let product: ProductModel
    
    // MARK: - State properties.
    @State private var isAnimating = false
    
    // MARK: - Body.
    var body: some View {
        HStack(alignment: .center, spacing: 8) {
            // Price.
            VStack(alignment: .leading, spacing: 8) {
                Text("Price")
                    .fontWeight(.semibold)
                
                Text(self.product.formattedPrice)
                    .font(.largeTitle)
                    .fontWeight(.black)
                    .scaleEffect(1.35, anchor: .leading)
            } // : VStack.
            .offset(y: self.isAnimating ? -48 : -72)
            
            Spacer()
            
            // Photo.
            Image(self.product.image)
                .resizable()
                .scaledToFit()
                .offset(y: self.isAnimating ? 0 : -32)
        } // : HStack.
        .onAppear(perform: {
            withAnimation(.easeOut(duration: 0.75)) {
                self.isAnimating.toggle()
            }
        })
    }
}

// MARK: - Preview.
#Preview {
    ProductTopDetailView(product: ProductModel.productList[0])
        .previewLayout(.sizeThatFits)
        .padding()
}
