//
//  ProductHeaderView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct ProductHeaderView: View {
    // MARK: - Properties.
    let product: ProductModel
    
    // MARK: - Body.
    var body: some View {
        VStack(alignment: .leading, spacing: 8) {
            Text("Protective Gear")
            
            Text(self.product.name)
                .font(.largeTitle)
                .fontWeight(.black)
        } // : VStack.
        .foregroundStyle(Color.tdDetailNavBarForegroundColor)
    }
}

// MARK: - Preview.
#Preview {
    ProductHeaderView(product: ProductModel.productList[0])
        .previewLayout(.sizeThatFits)
        .padding()
        .background(Color.tdDetailBackgroundColor)
}
