//
//  ProductBottomDetailView.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct ProductBottomDetailView: View {
    // MARK: - Properties.
    let product: ProductModel
    
    // MARK: - Body.
    var body: some View {
        VStack(alignment: .center, spacing: 0) {
            // Rating + sizes.
            ProductRatingSizesView()
                .padding(.top, -24)
                .padding(.bottom, 10)
            
            // Description.
            ScrollView(.vertical, showsIndicators: false) {
                Text(self.product.description)
                    .font(.system(.body, design: .rounded))
                    .foregroundStyle(Color.tdDetailForegroundColor)
                    .multilineTextAlignment(.leading)
            }
            
            // Quantity + favorite.
            ProductDetailQuantityView()
                .padding(.vertical, 8)
            
            // Add to cart.
            ProductDetailAddToCartView(product: self.product)
                .padding(.bottom, 24)
        } // : VStack.
        .padding(.horizontal)
        .background(
            Color.tdDetailCardBackgroundColor
                .clipShape(TopRoundedShape())
                .padding(.top, -104)
        )
    }
}

// MARK: - Preview.
#Preview {
    ProductBottomDetailView(product: ProductModel.productList[0])
}
