//
//  ContentView.swift
//  Touchdown
//
//  Created by Gastón Montes on 27/06/2024.
//

import SwiftUI
import SwiftData

struct ContentView: View {
    // MARK: - Properties.
    @StateObject private var shopViewModel = ShopViewModel()
    
    // MARK: - Body.
    var body: some View {
        ZStack {
            if !self.shopViewModel.showingProduct 
                && self.shopViewModel.selectedProduct == nil {
                VStack(spacing: 0) {
                    NavigationBarView()
                        .padding()
                        .background(.white)
                        .shadow(color: .black.opacity(0.05), radius: 5, x: 0, y:5)
                    
                    ScrollView(.vertical, showsIndicators: false) {
                        VStack(spacing: 0) {
                            FeatureTabView(players: PlayerModel.playerList)
                                .frame(height: UIScreen.main.bounds.width / 1.475)
                                .padding(.vertical, 20)
                            
                            CategoryGridView(categories: CategoryModel.categoryList)
                            
                            ProductGridView(shopViewModel: self.shopViewModel)
                            
                            BrandGridView(brands: BrandModel.brandList)
                            
                            FooterView()
                                .padding(.horizontal)
                        } // : VStack.
                    } // : ScrollView.
                }  // : VStack
            } else {
                ProductDetailsView(shopViewModel: self.shopViewModel)
            }
        } // : ZStack
    }
    
    func imageView(name: String) -> some View {
        Image(name)
            .aspectRatio(contentMode: .fill)
    }
}

// MARK: - Preview.
#Preview {
    ContentView()
}
