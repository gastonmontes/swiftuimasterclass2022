//
//  ProductModel+Color.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

extension ProductModel {
    var productColor: Color {
        return Color(red: self.color[0], green: self.color[1], blue: self.color[2])
    }
}
