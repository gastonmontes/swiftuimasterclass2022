//
//  Color+Touchdown.swift
//  Touchdown
//
//  Created by Gastón Montes on 28/06/2024.
//

import SwiftUI

extension Color {
    public static var tdColorBackground: Color {
        return Color("ColorBackground")
    }
    
    public static var tdColorGray: Color {
        return Color(UIColor.systemGray4)
    }
    
    public static var tdGridSectionColor: Color {
        return Color.white
    }
    
    public static var tdGridItemColor: Color {
        return Color.gray
    }
    
    public static var tdGridItemBackgroundColor: Color {
        return Color.white
    }
    
    public static var tdDetailBackgroundColor: Color {
        return Color.gray
    }
    
    public static var tdDetailForegroundColor: Color {
        return Color.gray
    }
    
    public static var tdDetailCardBackgroundColor: Color {
        return Color.white
    }
    
    public static var tdDetailNavBarForegroundColor: Color {
        return Color.white
    }
    
    public static var tdQuantityDetailForegroundColor: Color {
        return Color.black
    }
    
    public static var tdQuantityDetailFavoriteColor: Color {
        return Color.pink
    }
    
    public static var tdCartDetailForegroundColor: Color {
        return Color.white
    }
}
