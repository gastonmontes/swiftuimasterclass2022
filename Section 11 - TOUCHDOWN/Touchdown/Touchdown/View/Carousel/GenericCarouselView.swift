//
//  GenericCarouselView.swift
//  Touchdown
//
//  Created by Gastón Montes on 02/07/2024.
//

import SwiftUI

struct GenericCarouselView: View {
    // MARK - Properties.
    // TODO: - Gastón - Ver de pasar esto a un ViewModel.
    @GestureState private var dragState = DragState.inactive
    @State var carousalCurrentItemIndex = 0
    
    // Parameters which we will pass when call the CarousalView
    var itemHeight: CGFloat
    var views: [AnyView]
    
    // MARK - Body.
    var body: some View {
        ZStack {
            ForEach(0..<views.count) { i in
                self.views[i]
                    .frame(width: 300, height: getHeight(i))
                    .animation(.interpolatingSpring(stiffness: 300, damping: 30, initialVelocity: 10))
                    .background(.white)
                    .cornerRadius(15)
                    .opacity(getOpacity(i))
                    .offset(x: getOffset(i))
                    .animation(.interpolatingSpring(stiffness: 300, damping: 30, initialVelocity: 10))
            }
        } // : ZStack.
        .gesture(
            DragGesture()
                .updating($dragState) { drag, state, transaction in
                    state = .dragging(translation: drag.translation)
                }
                .onEnded(onDragEnded)
        )
    }
    
    // MARK - Drag functions.
    private func onDragEnded(drag: DragGesture.Value) {
        let dragThreshold: CGFloat = 200
        if drag.predictedEndTranslation.width > dragThreshold || drag.translation.width > dragThreshold {
            carousalCurrentItemIndex = carousalCurrentItemIndex - 1
        } else if (drag.predictedEndTranslation.width) < (-1 * dragThreshold) || (drag.translation.width) < (-1 * dragThreshold) {
            carousalCurrentItemIndex = carousalCurrentItemIndex + 1
        }
    }
    
    private func currentItemRelativeLocation() -> Int {
        let index = carousalCurrentItemIndex
        print(index)
        
        switch index {
        case Int.min..<0:
            return 0
        case _ where index >= self.views.count:
            return self.views.count - 1
        default:
            return index
        }
    }
    
    func getOffset(_ i: Int) -> CGFloat {
        let relativeLoc = self.currentItemRelativeLocation()
        
        if i == relativeLoc {
            return dragState.translation.width
        } else if i == relativeLoc + 1 || relativeLoc == views.count - 1 && i == 0{
            return dragState.translation.width + (300 + 20)
        } else if i == relativeLoc - 1 || relativeLoc == 0 && i == views.count - 1 {
            return dragState.translation.width - (300 + 20)
        } else if i == relativeLoc + 2 || (relativeLoc == views.count - 1 && i == 1) || (relativeLoc == views.count - 2 && i == 0) {
            return dragState.translation.width + (2*(300 + 20))
        } else if i == relativeLoc - 2 || (relativeLoc == 1 && i == views.count - 1) || (relativeLoc == 0 && i == views.count - 2) {
            return dragState.translation.width - (2*(300 + 20))
        } else if i == relativeLoc + 3 || (relativeLoc == views.count - 1 && i == 2) || (relativeLoc == views.count - 2 && i == 1) || (relativeLoc == views.count - 3 && i == 0){
            return dragState.translation.width + (3*(300 + 20))
        } else if i == relativeLoc - 3 || (relativeLoc == 2 && i == views.count - 1) || (relativeLoc == 1 && i == views.count - 2) || (relativeLoc == 0 && i == views.count - 3) {
            return dragState.translation.width - (3*(300 + 20))
        } else {
            return 10000
        }
    }
    
    func getHeight(_ i: Int) -> CGFloat {
        let relativeLoc = self.currentItemRelativeLocation()
        
        if i == relativeLoc {
            return itemHeight
        }
        
        return itemHeight - 100
    }
    
    func getOpacity(_ i: Int) -> Double {
        let relativeLoc = self.currentItemRelativeLocation()
        
        if i == relativeLoc
            || i + 1 == relativeLoc
            || i - 1 == relativeLoc
            || i + 2 == relativeLoc
            || i - 2 == relativeLoc
            || (i + 1) - views.count == relativeLoc
            || (i - 1) - views.count == relativeLoc
            || (i + 2) - views.count == relativeLoc
            || (i - 2) - views.count == relativeLoc {
            return 1
        }
        
        return 0
    }
    
    func imageView(name: String) -> some View {
        Image(name)
            .aspectRatio(contentMode: .fill)
    }
}

// MARK - Preview.
struct GenericCarouselView_Previews: PreviewProvider {
    static var previews: some View {
        GenericCarouselView(
            itemHeight: 500,
            views: [
                AnyView(Image("american-football-player-no1").aspectRatio(contentMode: .fill)),
                AnyView(Image("american-football-player-no2").aspectRatio(contentMode: .fill)),
                AnyView(Image("american-football-player-no3").aspectRatio(contentMode: .fill)),
                AnyView(Image("american-football-player-no4").aspectRatio(contentMode: .fill)),
                AnyView(Image("american-football-player-no5").aspectRatio(contentMode: .fill))
            ]
        )
        .previewLayout(.sizeThatFits)
    }
}
