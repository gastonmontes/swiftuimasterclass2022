//
//  AnotherCarouselView.swift
//  Touchdown
//
//  Created by Gastón Montes on 02/07/2024.
//

import SwiftUI

struct TeasingTabView: View {
    @Binding var selectedTab: Int
    let spacing: CGFloat
    let views: () -> [AnyView]
    
    @State private var offset = CGFloat.zero
    var viewCount: Int { views().count }

    var body: some View {
        VStack(spacing: spacing) {
            GeometryReader { geo in
                let width = geo.size.width * 0.75
                
                LazyHStack(spacing: spacing) {
                    Color.clear
                        .frame(width: geo.size.width * 0.15 - spacing)
                    ForEach(0..<viewCount, id: \.self) { idx in
                        views()[idx]
                            .frame(width: width, height: geo.size.height * self.getHeightMultiplier(idx))
                            .padding(.vertical)
                   }
                }
                .offset(x: CGFloat(-selectedTab) * (width + spacing) + offset)
                .animation(.easeOut, value: selectedTab)
                .gesture(
                    DragGesture()
                        .onChanged { value in
                            offset = value.translation.width
                        }
                        .onEnded { value in
                            withAnimation(.easeOut) {
                                offset = value.predictedEndTranslation.width
                                selectedTab -= Int((offset / width).rounded())
                                selectedTab = max(0, min(selectedTab, viewCount-1))
                                offset = 0
                            }
                        }
                )
            }
            //
            HStack {
                ForEach(0..<viewCount, id: \.self) { idx in
                    Circle().frame(width: 8)
                        .foregroundColor(idx == selectedTab ? .primary : .secondary.opacity(0.5))
                        .onTapGesture {
                            selectedTab = idx
                        }
                }
            }
        }
    }
    
    func getHeightMultiplier(_ i: Int) -> CGFloat {
        if i == self.selectedTab {
            return 1
        }
        
        return 0.85
    }
}


struct TabContentView: View {
    
    let title: String
    let color: Color
    
    var body: some View {
        Text(title).font(.title)
            .padding()
            .frame(maxWidth: .infinity, maxHeight: .infinity)
            .background(color.opacity(0.4), ignoresSafeAreaEdges: .all)
            .clipShape(RoundedRectangle(cornerRadius: 20))
    }
}
