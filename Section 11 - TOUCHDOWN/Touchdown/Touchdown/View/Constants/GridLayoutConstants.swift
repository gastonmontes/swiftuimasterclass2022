//
//  GridLayoutConstants.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct GridLayoutConstants {
    static let spacingColumn: CGFloat = 8
    static let spacingRow: CGFloat = 8
    
    static var items: [GridItem] {
        return Array(repeating: GridItem(.flexible(), spacing: spacingRow), count: 2)
    }
}
