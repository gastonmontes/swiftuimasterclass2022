//
//  HapticFeedBack.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/24/24.
//

import SwiftUI

class FeedbackHaptick {
    // MARK: - Singleton property.
    static let shared = FeedbackHaptick()
    
    // MARK: - Properties.
    private let feedbackHaptick = UIImpactFeedbackGenerator(style: .medium)
    
    // MARK: - Initialization.
    private init() {}
    
    // MARK: - Functions.
    func feedbackImpacted() {
        self.feedbackHaptick.impactOccurred()
    }
}
