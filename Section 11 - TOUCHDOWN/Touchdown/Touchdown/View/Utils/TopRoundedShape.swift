//
//  TopRoundedShape.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import SwiftUI

struct TopRoundedShape: Shape {
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect,
                                byRoundingCorners: [.topLeft, .topRight],
                                cornerRadii: CGSize(width: 32, height: 32))
        
        return Path(path.cgPath)
    }
}

// MARK: - Preview.
#Preview {
    TopRoundedShape()
        .previewLayout(.fixed(width: 432, height: 120))
        .padding()
}
