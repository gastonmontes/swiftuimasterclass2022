//
//  PlayerModel.swift
//  Touchdown
//
//  Created by Gastón Montes on 28/06/2024.
//

import Foundation

struct PlayerModel: Codable, Identifiable {
    let id: Int
    let image: String
    
    static let playerList: [PlayerModel] = Bundle.main.decode("player.json")
}
