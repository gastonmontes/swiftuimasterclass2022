//
//  CategoryModel.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import Foundation

struct CategoryModel: Codable, Identifiable {
    let id: Int
    let name: String
    let image: String
    
    static let categoryList: [CategoryModel] = Bundle.main.decode("category.json")
}
