//
//  BrandModel.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import Foundation

struct BrandModel: Codable, Identifiable {
    let id: Int
    let image: String
    
    static let brandList: [BrandModel] = Bundle.main.decode("brand.json")
}
