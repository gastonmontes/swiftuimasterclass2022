//
//  ProductModel.swift
//  Touchdown
//
//  Created by Gastón Montes on 7/23/24.
//

import Foundation

struct ProductModel: Codable, Identifiable {
    let id: Int
    let name: String
    let image: String
    let price: Int
    let description: String
    let color: [Double]
    
    var formattedPrice: String {
        return "$\(self.price)"
    }
    
    static let productList: [ProductModel] = Bundle.main.decode("product.json")
}
