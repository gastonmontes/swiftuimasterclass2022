//
//  PageData.swift
//  Pinch
//
//  Created by Gastón Montes on 29/03/2022.
//

import Foundation

let pagesData: [PageModel] = [
    PageModel(id: 0, imageName: "magazine-front-cover"),
    PageModel(id: 1, imageName: "magazine-back-cover")
]
