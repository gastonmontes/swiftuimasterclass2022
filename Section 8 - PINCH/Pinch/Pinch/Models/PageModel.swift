//
//  PageModel.swift
//  Pinch
//
//  Created by Gastón Montes on 29/03/2022.
//

import Foundation

struct PageModel: Identifiable {
    let id: Int
    let imageName: String
}

extension PageModel {
    var thumbnailName: String {
        return "thumb-" + self.imageName
    }
}
