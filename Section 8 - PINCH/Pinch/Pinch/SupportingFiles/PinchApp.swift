//
//  PinchApp.swift
//  Pinch
//
//  Created by Gastón Montes on 29/03/2022.
//

import SwiftUI

@main
struct PinchApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
