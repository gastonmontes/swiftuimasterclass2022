//
//  InfoPanelView.swift
//  Pinch
//
//  Created by Gastón Montes on 29/03/2022.
//

import SwiftUI

struct InfoPanelView: View {
    var scale: CGFloat
    var offset: CGSize
    
    @State private var isInfoPanelVisible: Bool = false
    
    var body: some View {
        HStack {
            // MARK: - HotSpot.
            Image(systemName: "circle.circle")
                .symbolRenderingMode(.hierarchical)
                .resizable()
                .frame(width: 30, height: 30)
                .onLongPressGesture(minimumDuration: 1) {
                    withAnimation(.easeOut) {
                        self.isInfoPanelVisible.toggle()
                    }
                }
            
            Spacer()
            
            // MARK: - Info panel.
            HStack(spacing: 2) {
                Image(systemName: "arrow.up.left.and.arrow.down.right")
                Text("\(self.scale)")
                
                Spacer()
                
                Image(systemName: "arrow.left.and.right")
                Text("\(self.offset.width)")
                
                Spacer()
                
                Image(systemName: "arrow.up.and.down")
                Text("\(self.offset.height)")
            }
            .font(.footnote)
            .padding(8)
            .background(.ultraThinMaterial)
            .cornerRadius(8)
            .frame(maxWidth: 420)
            .opacity(self.isInfoPanelVisible ? 1 : 0)
            
            Spacer()
        }
    }
}

struct InfoPanelView_Previews: PreviewProvider {
    static var previews: some View {
        InfoPanelView(scale: 1, offset: .zero)
            .preferredColorScheme(.dark)
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
