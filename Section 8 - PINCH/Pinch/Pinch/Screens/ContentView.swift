//
//  ContentView.swift
//  Pinch
//
//  Created by Gastón Montes on 29/03/2022.
//

import SwiftUI

struct ContentView: View {
    // MARK: - Property.
    @State private var isAnimating: Bool = false
    @State private var imageScale: CGFloat = 1
    @State private var imageOffset: CGSize = .zero
    @State private var isDrawerOpen: Bool = false
    
    private let pages: [PageModel] = pagesData
    @State private var pageIndex: Int = 0
    
    // MARK: - Functions.
    private func resetImageState() {
        return withAnimation(.spring()) {
            self.imageScale = 1
            self.imageOffset = .zero
        }
    }
    
    private func currentPageImageName() -> String {
        return self.pages[self.pageIndex].imageName
    }
    
    // MARK: - Content.
    var body: some View {
        NavigationView {
            ZStack {
                Color.clear
                
                // MARK: - Front Image.
                Image(self.currentPageImageName())
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .cornerRadius(10)
                    .padding()
                    .shadow(color: .black.opacity(0.2),
                            radius: 12,
                            x: 2,
                            y: 2)
                    .opacity(self.isAnimating ? 1 : 0)
                    .offset(x: self.imageOffset.width,
                            y: self.imageOffset.height)
                    .scaleEffect(self.imageScale)
                    .onTapGesture(count: 2, perform: {
                        if self.imageScale == 1 {
                            withAnimation(.spring()) {
                                self.imageScale = 5
                            }
                        } else {
                            self.resetImageState()
                        }
                    })
                // MARK: - Drag Gesture.
                    .gesture(
                        DragGesture()
                            .onChanged({ value in
                                withAnimation(.linear(duration: 1)) {
                                    self.imageOffset = value.translation
                                }
                            })
                            .onEnded( { _ in
                                if self.imageScale <= 1 {
                                    self.resetImageState()
                                }
                            })
                    )
                // MARK: - Magnification Gesture.
                    .gesture(
                        MagnificationGesture()
                            .onChanged { value in
                                withAnimation(.linear(duration: 1)) {
                                    if value >= 0.125 && value <= 5 {
                                        self.imageScale = value
                                    } else if value > 5 {
                                        self.imageScale = 5
                                    } else if value < 0.125 {
                                        self.imageScale = 0.125
                                    }
                                }
                            }
                            .onEnded { _ in
                                if self.imageScale > 5 {
                                    self.imageScale = 5
                                } else if self.imageScale < 0.125 {
                                    self.imageScale = 0.125
                                }
                            }
                    )
            }
            .navigationTitle("Pinch & Zoom")
            .navigationBarTitleDisplayMode(.inline)
            .onAppear(perform: {
                withAnimation(.linear(duration: 1)) {
                    self.isAnimating = true
                }
            })
            // MARK: - Info Panel.
            .overlay(
                InfoPanelView(scale: self.imageScale, offset: self.imageOffset)
                    .padding(.horizontal)
                    .padding(.top, 30)
                , alignment: .top
            )
            // MARK: - Controls.
            .overlay(
                Group {
                    HStack {
                        // MARK: - Scale down.
                        Button {
                            withAnimation(.spring()) {
                                let newScale = self.imageScale * 0.875
                                self.imageScale = (newScale >= 0.125) ? newScale : 0.125
                            }
                        } label: {
                            ControlImageView(icon: "minus.magnifyingglass")
                        }
                        
                        // MARK: - Reset.
                        Button {
                            self.resetImageState()
                        } label: {
                            ControlImageView(icon: "arrow.up.left.and.down.right.magnifyingglass")
                        }
                        
                        // MARK: - Scale up.
                        Button {
                            withAnimation(.spring()) {
                                let newScale = self.imageScale * 1.125
                                self.imageScale = (newScale <= 5) ? newScale : 5
                            }
                        } label: {
                            ControlImageView(icon: "plus.magnifyingglass")
                        }
                    }
                    .padding(EdgeInsets(top: 12, leading: 20, bottom: 12, trailing: 20))
                    .background(.ultraThinMaterial)
                    .cornerRadius(12)
                    .opacity(self.isAnimating ? 1 : 0)
                }
                    .padding(.bottom, 30)
                , alignment: .bottom
            )
            // MARK: - Drawer.
            .overlay(
                HStack(spacing: 12) {
                    // MARK: - Drawer handler.
                    let drawerArrowName = self.isDrawerOpen ? "chevron.compact.right" : "chevron.compact.left"
                    Image(systemName: drawerArrowName)
                        .resizable()
                        .scaledToFit()
                        .frame(height: 40)
                        .padding(8)
                        .foregroundStyle(.secondary)
                        .onTapGesture(perform: {
                            withAnimation(.easeOut) {
                                self.isDrawerOpen.toggle()
                            }
                        })
                    
                    // MARK: - Thumbnails.
                    ForEach(self.pages) { page in
                        Image(page.thumbnailName)
                            .resizable()
                            .scaledToFit()
                            .frame(width: 80)
                            .cornerRadius(8)
                            .shadow(radius: 4)
                            .opacity(self.isDrawerOpen ? 1 : 0)
                            .animation(.easeOut(duration:0.5), value: self.isDrawerOpen)
                            .onTapGesture(perform: {
                                self.isAnimating = true
                                
                                if self.pageIndex != page.id {
                                    self.pageIndex = page.id
                                    self.resetImageState()
                                }
                            })
                    }
                    
                    Spacer()
                }
                    .padding(EdgeInsets(top: 16, leading: 8, bottom: 16, trailing: 8))
                    .background(.ultraThinMaterial)
                    .cornerRadius(12)
                    .opacity(self.isAnimating ? 1 : 0)
                    .frame(width: 260)
                    .padding(.top, UIScreen.main.bounds.height / 12)
                    .offset(x: self.isDrawerOpen ? 20 : 215)
                , alignment: .topTrailing
            )
        }
        .navigationViewStyle(.stack)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .preferredColorScheme(.dark)
    }
}
