//
//  LinksApp.swift
//  Links
//
//  Created by Gastón Montes on 29/09/2023.
//

import SwiftUI

@main
struct LinksApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
