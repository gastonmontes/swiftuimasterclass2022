//
//  ContentView.swift
//  Links
//
//  Created by Gastón Montes on 29/09/2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Link("Go to Apple", destination: URL(string: "https://apple.com")!)
                .buttonStyle(.borderless)
            
            Link("Call to Action", destination: URL(string: "tel:1234567890")!)
                .buttonStyle(.bordered)
                .buttonBorderShape(.roundedRectangle)
                .controlSize(.regular)
            
            Link("Send an Email", destination: URL(string: "mailto:gastonmontes@hotmail.com")!)
                .buttonStyle(.borderedProminent)
                .buttonBorderShape(.capsule)
                .controlSize(.large)
                .tint(.pink)
            
            Link("Credo Academy", destination: URL(string: "https://credo.academy")!)
                .buttonStyle(.plain)
                .padding()
                .border(.primary, width: 2)
            
            Link(destination: URL(string: "https://apple.com")!) {
                HStack(spacing: 16) {
                    Image(systemName: "apple.logo")
                    Text("Apple Store")
                }
                .font(.largeTitle)
                .foregroundColor(.white)
                .padding()
                .background(Capsule().fill(.blue))
            }
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
