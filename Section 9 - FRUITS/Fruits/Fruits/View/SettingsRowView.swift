//
//  SettingsRowView.swift
//  Fruits
//
//  Created by Gastón Montes on 19/07/2022.
//

import SwiftUI

struct SettingsRowView: View {
    // MARK: - Properties.
    var name: String
    var content: String? = nil
    var linkLabel: String? = nil
    var linkDestination: String? = nil
    
    // MARK: - Body.
    var body: some View {
        VStack {
            Divider().padding(.vertical, 4)
            
            HStack {
                Text(self.name).foregroundColor(.gray)
                
                Spacer()
                
                if let content = self.content {
                    Text(content)
                } else if let linkLabel = self.linkLabel, let linkDest = self.linkDestination {
                    Link(linkLabel, destination: URL(string: "https://\(linkDest)")!)
                    Image(systemName: "arrow.up.right.square").foregroundColor(.pink)
                } else {
                    /*@START_MENU_TOKEN@*/EmptyView()/*@END_MENU_TOKEN@*/
                }
            }
        } // : HStack.
    }
}

// MARK: - Preview.
struct SettingsRowView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SettingsRowView(name: "Developer", content: "Gastón Montes")
                .previewLayout(.fixed(width: 375, height: 60))
            .padding()
            
            SettingsRowView(name: "Website",
                            linkLabel: "SwiftUI Masterclass",
                            linkDestination: "swiftuimasterclass.com")
                .preferredColorScheme(.dark)
                .previewLayout(.fixed(width: 375, height: 60))
                .padding()
        }
    }
}
