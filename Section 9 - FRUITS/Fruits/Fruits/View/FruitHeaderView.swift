//
//  FruitHeaderView.swift
//  Fruits
//
//  Created by Gastón Montes on 19/07/2022.
//

import SwiftUI

struct FruitHeaderView: View {
    // MARK: - Properties.
    var fruit: Fruit
    
    @State private var isAnimatingImage = false
    
    // MARK: - Body.
    var body: some View {
        ZStack {
            LinearGradient(gradient: Gradient(colors: self.fruit.gradientColors),
                           startPoint: .topLeading,
                           endPoint: .bottomLeading)
            
            Image(self.fruit.image)
                .resizable()
                .scaledToFit()
                .shadow(color: .black.opacity(0.15), radius: 8, x: 6, y: 8)
                .padding(.vertical, 20)
                .scaleEffect(self.isAnimatingImage ? 1.0 : 0.6)
        } // : ZStack.
        .frame(height: 440)
        .onAppear() {
            withAnimation(.easeOut(duration: 0.5)) {
                self.isAnimatingImage = true
            }
        }
    }
}

// MARK: - Preview.
struct FruitHeaderView_Previews: PreviewProvider {
    static var previews: some View {
        FruitHeaderView(fruit: fruitsData[0])
            .previewLayout(.fixed(width: 375, height: 440))
    }
}
