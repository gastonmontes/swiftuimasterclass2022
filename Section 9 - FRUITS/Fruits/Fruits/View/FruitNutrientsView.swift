//
//  FruitNutrientsView.swift
//  Fruits
//
//  Created by Gastón Montes on 19/07/2022.
//

import SwiftUI

struct FruitNutrientsView: View {
    // MARK: - Properties.
    var fruit: Fruit
    let nutrients: [ String ] = [ "Energy", "Sugar", "Fat", "Proteins", "Vitamins", "Minerals"]
    
    // MARK: - Body.
    var body: some View {
        GroupBox {
            DisclosureGroup("Nutritionals value per 100g") {
                ForEach(0..<self.nutrients.count, id: \.self) { item in
                    Divider()
                        .padding(.vertical, 2)
                    
                    HStack {
                        Group {
                            Image(systemName: "info.circle")
                            
                            Text(self.nutrients[item])
                        }
                        .foregroundColor(self.fruit.gradientColors[1])
                        .font(.system(.body).bold())
                        
                        Spacer(minLength: 25)
                        
                        Text(self.fruit.nutrition[item])
                            .multilineTextAlignment(.trailing)
                    } // : HStack
                }
            } // : DisclosureGroup.
        } // : GroupBox.
    }
}

// MARK: - Preview.
struct FruitNutrientsView_Previews: PreviewProvider {
    static var previews: some View {
        FruitNutrientsView(fruit: fruitsData[2])
            .preferredColorScheme(.dark)
            .previewLayout(.fixed(width: 375, height: 480))
            .padding()
    }
}
