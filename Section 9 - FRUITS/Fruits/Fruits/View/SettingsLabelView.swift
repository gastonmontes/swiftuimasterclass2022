//
//  SettingsLabelView.swift
//  Fruits
//
//  Created by Gastón Montes on 19/07/2022.
//

import SwiftUI

struct SettingsLabelView: View {
    // MARK: - Properties.
    var labelText: String
    var labelImage: String
    
    // MARK: - Body.
    var body: some View {
        HStack {
            Text(self.labelText.uppercased())
                .fontWeight(.bold)
            
            Spacer()
            
            Image(systemName: self.labelImage)
        } // : HStack.
    }
}

// MARK: - Preview.
struct SettingsLabelView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsLabelView(labelText: "Fructus", labelImage: "info.circle")
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
