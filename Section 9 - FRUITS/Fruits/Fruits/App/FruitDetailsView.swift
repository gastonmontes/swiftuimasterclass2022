//
//  FruitDetailsView.swift
//  Fruits
//
//  Created by Gastón Montes on 19/07/2022.
//

import SwiftUI

struct FruitDetailsView: View {
    // MARK: - Properties.
    var fruit: Fruit
    
    // MARK: - Body.
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(alignment: .center, spacing: 20) {
                    // Header.
                    FruitHeaderView(fruit: self.fruit)
                    
                    VStack(alignment: .leading, spacing: 20) {
                        // Title.
                        Text(self.fruit.title)
                            .font(.largeTitle)
                            .fontWeight(.heavy)
                            .foregroundColor(self.fruit.gradientColors[1])
                        
                        // Headline.
                        Text(self.fruit.headline)
                            .font(.headline)
                            .multilineTextAlignment(.leading)
                        
                        // Nutrients.
                        FruitNutrientsView(fruit: self.fruit)
                        
                        // Subheadline.
                        Text("Learn more about \(self.fruit.title)".uppercased())
                            .fontWeight(.bold)
                            .foregroundColor(self.fruit.gradientColors[1])
                        
                        // Description.
                        Text(self.fruit.description)
                            .multilineTextAlignment(.leading)
                        
                        // Link.
                        SourceLinkView()
                            .padding(.top, 10)
                            .padding(.bottom, 40)
                    } // : VStack
                    .padding(.horizontal, 20)
                    .frame(maxWidth: 640, alignment: .center)
                } // : VStack.
                .navigationBarHidden(true)
            } // : ScrollView.
            .edgesIgnoringSafeArea(.top)
        } // : Navigation.
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

// MARK: - Preview.
struct FruitDetailsView_Previews: PreviewProvider {
    static var previews: some View {
        FruitDetailsView(fruit: fruitsData[1])
    }
}
