//
//  OnboardingView.swift
//  Fruits
//
//  Created by Gastón Montes on 01/06/2022.
//

import SwiftUI

struct OnboardingView: View {
    // MARK: - Properties.
    var fruits = fruitsData
    
    // MARK: - Body.
    var body: some View {
        TabView {
            ForEach(self.fruits) { fruit in
                FruitCardView(fruit: fruit)
            }
        } // : Tabiew.
        .tabViewStyle(PageTabViewStyle())
        .padding(.vertical, 20)
    }
}

// MARK: - Preview.
struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView(fruits: fruitsData)
    }
}
