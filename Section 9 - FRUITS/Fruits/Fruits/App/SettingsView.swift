//
//  SettingsView.swift
//  Fruits
//
//  Created by Gastón Montes on 19/07/2022.
//

import SwiftUI

struct SettingsView: View {
    // MARK: - Properties.
    @Environment(\.presentationMode) var presentationMode
    
    @AppStorage("isOnboarding") var isOnboarding = false
    
    // MARK: - Body.
    var body: some View {
        NavigationView {
            ScrollView(.vertical, showsIndicators: false) {
                VStack(spacing: 20) {
                    // MARK: - Section 1.
                    GroupBox(
                        label:
                            SettingsLabelView(labelText: "Fructus", labelImage: "info.circle")
                    ) {
                        Divider().padding(.vertical, 4)
                        
                        HStack(alignment: .center, spacing: 10) {
                            Image("logo")
                                .resizable()
                                .scaledToFit()
                                .frame(width: 80, height: 80)
                                .cornerRadius(9)
                            
                            Text("Most fruits are naturally low in fat, sodium, and calories. None have colesterol. Fruits are sources of many essentials nutrients, including potassium, dietary fiber, vitamins, and much more.")
                                .font(.footnote)
                        } // : HStack.
                    } // : GroupBox.
                    
                    // MARK: - Section 2.
                    GroupBox(
                        label:
                            SettingsLabelView(labelText: "Customization", labelImage: "info.circle")
                    ) {
                        VStack {
                            Divider().padding(.vertical, 4)
                            
                            Text("if you wish, you can restart, you can restart the application by toggle the switch in this box. That way starts the onboarding process and you will see the welcome screen again.")
                                .padding(.vertical, 8)
                                .frame(minHeight: 60)
                                .layoutPriority(1)
                                .font(.footnote)
                                .multilineTextAlignment(.leading)
                            
                            Divider().padding(.vertical, 4)
                            
                            Toggle(isOn: self.$isOnboarding) {
                                if self.isOnboarding {
                                    Text("Restarted".uppercased())
                                        .fontWeight(.bold)
                                        .foregroundColor(.green)
                                } else {
                                    Text("Restart".uppercased())
                                        .fontWeight(.bold)
                                        .foregroundColor(.secondary)
                                }
                            }
                            .padding()
                            .background(
                                Color(UIColor.tertiarySystemBackground)
                                    .clipShape(RoundedRectangle(cornerRadius: 8, style: .continuous))
                            )
                        } // : VStack.
                    } // : GroupBox.
                    
                    // MARK: - Section 3.
                    GroupBox(
                        label:
                            SettingsLabelView(labelText: "Application", labelImage: "apps.iphone")
                    ) {                        
                        SettingsRowView(name: "Developer", content: "Gastón Montes")
                        SettingsRowView(name: "Designer", content: "Gastón Montes")
                        SettingsRowView(name: "Compatibility", content: "iOS 14")
                        SettingsRowView(name: "Website",
                                        linkLabel: "SwiftUI Masterclass",
                                        linkDestination: "swiftuimasterclass.com")
                        SettingsRowView(name: "Twitter",
                                        linkLabel: "@ElgatitoMontes",
                                        linkDestination: "twitter.com/ElgatitoMontes")
                        SettingsRowView(name: "SwiftUI", content: "2.0")
                        SettingsRowView(name: "Version", content: "1.1.0")
                    } // : GroupBox.
                } // : VStack.
                .navigationBarTitle(Text("Settings"), displayMode: .large)
                .navigationBarItems(
                    trailing:
                        Button(action: {
                            self.presentationMode.wrappedValue.dismiss()
                        }) {
                            Image(systemName: "xmark")
                        }
                )
                .padding()
            } // : ScrollView.
        } // : NavigationView.
    }
}

// MARK: - Preview.
struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView()
            .previewInterfaceOrientation(.portraitUpsideDown)
    }
}
