//
//  FruitsApp.swift
//  Fruits
//
//  Created by Gastón Montes on 01/06/2022.
//

import SwiftUI

@main
struct FruitsApp: App {
    @AppStorage("isOnboarding") var isOnboaring: Bool = true
    
    var body: some Scene {
        WindowGroup {
            if self.isOnboaring {
                OnboardingView()
            } else {
                ContentView()
            }
        }
    }
}
