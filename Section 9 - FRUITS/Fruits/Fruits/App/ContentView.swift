//
//  ContentView.swift
//  Fruits
//
//  Created by Gastón Montes on 01/06/2022.
//

import SwiftUI

struct ContentView: View {
    // MARK: - Properties.
    @State private var isShowingSettings = false
    
    var fruits: [ Fruit ] = fruitsData
    
    // MARK: - Body.
    var body: some View {
        NavigationView {
            List {
                ForEach(self.fruits.shuffled()) { item in
                    NavigationLink(destination: FruitDetailsView(fruit: item)) {
                        FruitRowView(fruit: item)
                            .padding(.vertical, 4)
                    }
                }
            }
            .navigationTitle("Fruits")
            .navigationBarItems(
                trailing:
                    Button(action: {
                        self.isShowingSettings = true
                    }) {
                        Image(systemName: "slider.horizontal.3")
                    } // : Button.
                    .sheet(isPresented: self.$isShowingSettings) {
                        SettingsView()
                    }
            )
        } // : Navigation.
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

// MARK: - Preview.
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(fruits: fruitsData)
    }
}
