//
//  GradientsApp.swift
//  Gradients
//
//  Created by Gastón Montes on 29/09/2023.
//

import SwiftUI

@main
struct GradientsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
