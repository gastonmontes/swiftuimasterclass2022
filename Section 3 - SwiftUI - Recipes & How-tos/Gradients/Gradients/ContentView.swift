//
//  ContentView.swift
//  Gradients
//
//  Created by Gastón Montes on 29/09/2023.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text("iOS")
                .font(.system(size: 180))
                .fontWeight(.black)
                .foregroundStyle(.teal.gradient)
            
            Text("iOS")
                .font(.system(size: 180))
                .fontWeight(.black)
                .foregroundStyle(
                    LinearGradient(
                        colors: [.pink, .purple, .blue],
                        startPoint: .topLeading,
                        endPoint: .bottomTrailing
                    )
                )
            
            Text("iOS")
                .font(.system(size: 180))
                .fontWeight(.black)
                .foregroundStyle(
                    LinearGradient(
                        colors: [.pink, .purple, .blue],
                        startPoint: .topTrailing,
                        endPoint: .bottomLeading
                    )
                )
            
            Text("iOS")
                .font(.system(size: 180))
                .fontWeight(.black)
                .foregroundStyle(
                    LinearGradient(
                        colors: [.pink, .green, .blue],
                        startPoint: .leading,
                        endPoint: .trailing
                    )
                )
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
