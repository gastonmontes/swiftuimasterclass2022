//
//  ContentView.swift
//  AsyncImages
//
//  Created by Gastón Montes on 29/09/2023.
//

import SwiftUI

extension Image {
    func imageModifier() -> some View {
        self
            .resizable()
            .scaledToFit()
    }
    
    func iconModifier() -> some View {
        self.imageModifier()
            .foregroundColor(.purple)
            .opacity(0.5)
            .frame(maxWidth: 128)
    }
}

struct ContentView: View {
    private let imageURL = URL(string: "https://credo.academy/credo-academy@3x.png")!
    private let imageURLFailure = URL(string: "https://credo.academy/credo-academy@3x.pn")!
    
    var body: some View {
        ScrollView {
            AsyncImage(url: self.imageURL,
                       transaction: Transaction(animation: .spring(response: 0.5,
                                                                   dampingFraction: 0.6,
                                                                   blendDuration: 0.25))) { phase in
                switch phase {
                case .success(let image):
                    image
                        .imageModifier()
//                        .transition(.move(edge: .leading))
//                        .transition(.slide)
                        .transition(.scale)
                case .empty:
                    Image(systemName: "photo.circle.fill").iconModifier()
                case .failure(_):
                    Image(systemName: "ant.circle.fill").iconModifier()
                default:
                    ProgressView()
                }
            }
            
            AsyncImage(url: self.imageURLFailure) { phase in
                if let image = phase.image {
                    image.imageModifier()
                } else if phase.error != nil {
                    Image(systemName: "ant.circle.fill").iconModifier()
                } else {
                    Image(systemName: "photo.circle.fill").iconModifier()
                }
            }
            
            AsyncImage(url: self.imageURL) { phase in
                if let image = phase.image {
                    image.imageModifier()
                } else if phase.error != nil {
                    Image(systemName: "ant.circle.fill").iconModifier()
                } else {
                    Image(systemName: "photo.circle.fill").iconModifier()
                }
            }
            
            AsyncImage(url: self.imageURL) { image in
                image.imageModifier()
            } placeholder: {
                Image(systemName: "photo.circle.fill").iconModifier()
            }

            AsyncImage(url: self.imageURL, scale: 3)
            
//            AsyncImage(url: self.imageURL)
        }
        .padding(40)
    }
}

#Preview {
    ContentView()
}
