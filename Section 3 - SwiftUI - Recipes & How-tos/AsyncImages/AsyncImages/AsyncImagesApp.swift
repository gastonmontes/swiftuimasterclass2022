//
//  AsyncImagesApp.swift
//  AsyncImages
//
//  Created by Gastón Montes on 29/09/2023.
//

import SwiftUI

@main
struct AsyncImagesApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
