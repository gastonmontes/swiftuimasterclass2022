//
//  ListRowItemView.swift
//  Devote
//
//  Created by Gastón Montes on 10/04/2022.
//

import SwiftUI

struct ListRowItemView: View {
    @Environment(\.managedObjectContext) var viewContext
    @ObservedObject var item: Item
    
    var body: some View {
        Toggle(isOn: $item.completion) {
            Text(self.item.task ?? "")
                .font(.system(.title, design: .rounded))
                .fontWeight(.heavy)
                .foregroundColor(self.item.completion ? .pink : .primary)
                .padding(.vertical, 12)
                .animation(.default, value: self.item.completion)
        }
        .toggleStyle(CheckBoxStyle())
        .onReceive(self.item.objectWillChange, perform: { _ in
            if self.viewContext.hasChanges {
                try? self.viewContext.save()
            }
        })
    }
}
