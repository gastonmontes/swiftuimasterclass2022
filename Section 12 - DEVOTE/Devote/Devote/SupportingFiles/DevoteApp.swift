//
//  DevoteApp.swift
//  Devote
//
//  Created by Gastón Montes on 07/04/2022.
//

import SwiftUI

@main
struct DevoteApp: App {
    let persistenceController = PersistenceController.shared
    @AppStorage("isDarkMode") var isDarkMode: Bool = false

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .preferredColorScheme(self.isDarkMode ? .dark : .light)
        }
    }
}
