//
//  Contants.swift
//  Devote
//
//  Created by Gastón Montes on 08/04/2022.
//

import Foundation
import SwiftUI

// MARK: - Formatter.
let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .medium
    return formatter
}()

// MARK: - UI.
var backgroundGradient: LinearGradient {
    return LinearGradient(gradient: Gradient(colors: [ Color.pink, Color.blue ]),
                          startPoint: .topLeading,
                          endPoint: .bottomTrailing)
}

// MARK: - UX.
let feedback = UINotificationFeedbackGenerator()
