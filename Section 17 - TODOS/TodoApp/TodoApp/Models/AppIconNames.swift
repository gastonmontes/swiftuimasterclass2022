//
//  AppIconNames.swift
//  TodoApp
//
//  Created by Gastón Montes on 15/04/2022.
//

import Foundation
import UIKit

class AppIconNames: ObservableObject {
    private(set) var iconNames: [ String? ] = [ nil ]
    @Published var currentIndex = 0
    
    init() {
        self.getAlternateIconsNames()
        
        if let currentIcon = UIApplication.shared.alternateIconName {
            self.currentIndex = self.iconNames.firstIndex(of: currentIcon) ?? 0
        }
    }
    
    func getAlternateIconsNames() {
        if let icons = Bundle.main.object(forInfoDictionaryKey: "CFBundleIcons") as? [ String : Any ],
           let alternateIcons = icons["CFBundleAlternateIcons"] as? [ String : Any ] {
            for (_, value) in alternateIcons {
                guard let iconList = value as? [ String : Any ],
                      let iconFiles = iconList["CFBundleIconFiles"] as? [ String ],
                      let icon = iconFiles.first else {
                    return
                }
                
                self.iconNames.append(icon)
            }
        }
    }
}
