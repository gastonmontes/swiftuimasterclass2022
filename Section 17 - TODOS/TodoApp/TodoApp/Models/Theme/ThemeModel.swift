//
//  ThemeModel.swift
//  TodoApp
//
//  Created by Gastón Montes on 15/04/2022.
//

import SwiftUI

struct ThemeModel: Identifiable {
    let id: Int
    let themeName: String
    let themeColor: Color
}
