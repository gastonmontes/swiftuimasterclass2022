//
//  ThemeSettings.swift
//  TodoApp
//
//  Created by Gastón Montes on 15/04/2022.
//

import Foundation

final public class ThemeSettings: ObservableObject {
    @Published public var themeSettings: Int = UserDefaults.standard.integer(forKey: "Theme") {
        didSet {
            UserDefaults.standard.set(self.themeSettings, forKey: "Theme")
        }
    }
    
    private init() {
        
    }
    
    public static let shared = ThemeSettings()
}
