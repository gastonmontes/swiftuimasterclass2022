//
//  ThemeData.swift
//  TodoApp
//
//  Created by Gastón Montes on 15/04/2022.
//

import Foundation

let themeData: [ ThemeModel ] = [
    ThemeModel(id: 0, themeName: "Pink theme", themeColor: .pink),
    ThemeModel(id: 1, themeName: "Blue theme", themeColor: .blue),
    ThemeModel(id: 2, themeName: "Green theme", themeColor: .green)
]
