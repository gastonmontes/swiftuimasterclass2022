//
//  AddTodoView.swift
//  TodoApp
//
//  Created by Gastón Montes on 11/04/2022.
//

import SwiftUI

struct AddTodoView: View {
    // MARK: - Properties.
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.managedObjectContext) private var managedObjectContext
    
    @State private var name: String = ""
    @State private var priority: String = "Normal"
    
    private let priorities = [ "High", "Normal", "low" ]
    
    @State private var errorShowing: Bool = false
    @State private var errorTitle: String = ""
    @State private var errorMessage: String = ""
    
    let themes = themeData
    @ObservedObject var theme = ThemeSettings.shared
    
    // MARK: - Body.
    var body: some View {
        NavigationView {
            VStack {
                VStack(alignment: .leading, spacing: 20) {
                    // MARK: - TODO name.
                    TextField("Todo", text: $name)
                        .padding()
                        .background(Color(UIColor.quaternarySystemFill))
                        .cornerRadius(9)
                        .font(.system(size: 24, weight: .bold, design: .default))
                    
                    Picker("Priority", selection: $priority) {
                        ForEach(self.priorities, id: \.self) {
                            Text($0)
                        }
                    } // : Picker.
                    .pickerStyle(SegmentedPickerStyle())
                    
                    // MARK: - Save button.
                    Button(action: {
                        if !self.name.isEmpty {
                            let todo = Todo(context: self.managedObjectContext)
                            todo.id = UUID()
                            todo.name = self.name
                            todo.priority = self.priority
                            
                            do {
                                try self.managedObjectContext.save()
                            } catch {
                                print(error)
                            }
                        } else {
                            self.errorShowing = true
                            self.errorTitle = "Invalid name"
                            self.errorMessage = "Make sure to enter something for\nthe new todo item."
                            return
                        }
                        
                        self.presentationMode.wrappedValue.dismiss()
                    }) {
                        Text("Save")
                            .font(.system(size: 24, weight: .bold, design: .default))
                            .padding()
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .cornerRadius(9)
                            .foregroundColor(Color.white)
                            .background(self.themes[self.theme.themeSettings].themeColor)
                    } // : Save button.
                    
                } // : VStack.
                .padding(.horizontal)
                .padding(.vertical, 30)
                
                Spacer()
            } // : VStack.
            .navigationBarTitle("New Todo", displayMode: .inline)
            .navigationBarItems(trailing: Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark")
            })
            .alert(isPresented: $errorShowing) {
                Alert(title: Text(self.errorTitle), message: Text(self.errorMessage), dismissButton: .default(Text("OK")))
            }
        } // : Navigation.
        .accentColor(self.themes[self.theme.themeSettings].themeColor)
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

struct AddTodoView_Previews: PreviewProvider {
    static var previews: some View {
        AddTodoView()
    }
}
