//
//  SettingsView.swift
//  TodoApp
//
//  Created by Gastón Montes on 15/04/2022.
//

import SwiftUI

struct SettingsView: View {
    // MARK: - Properties.
    @Environment(\.presentationMode) private var presentationMode
    @EnvironmentObject private var appIconNames: AppIconNames
    
    let themes = themeData
    @ObservedObject var theme = ThemeSettings.shared
    @State private var isThemeChanged: Bool = false
    
    // MARK: - Body.
    var body: some View {
        NavigationView {
            VStack(alignment: .center, spacing: 0) {
                // MARK: - Form.
                Form {
                    // MARK: - Section 1.
                    Section(header: Text("Choose the app icon")) {
                        Picker(selection: $appIconNames.currentIndex, label:
                                HStack {
                            ZStack {
                                RoundedRectangle(cornerRadius: 8, style: .continuous)
                                    .strokeBorder(Color.primary, lineWidth: 2)
                                
                                Image(systemName: "paintbrush")
                                    .font(.system(size: 28, weight: .regular, design: .default))
                                    .foregroundColor(Color.primary)
                            }
                            .frame(width: 44, height: 44)
                            
                            Text("App icons")
                                .foregroundColor(Color.primary)
                        } // : HStack.
                               
                        ) {
                            ForEach(0..<self.appIconNames.iconNames.count) { index in
                                HStack {
                                    let image = UIImage(named: self.appIconNames.iconNames[index] ?? "Blue") ?? UIImage()
                                    Image(uiImage: image)
                                        .renderingMode(.original)
                                        .resizable()
                                        .scaledToFit()
                                        .frame(width: 48, height: 48)
                                        .cornerRadius(9)
                                    
                                    Spacer().frame(width: 8)
                                    
                                    Text(self.appIconNames.iconNames[index] ?? "Blue")
                                        .frame(alignment: .leading)
                                } // : HStack.
                                .padding(3)
                            }
                        } // : Picker.
                        .onReceive([self.appIconNames.currentIndex].publisher.first()) { value in
                            let index = self.appIconNames.iconNames.firstIndex(of: UIApplication.shared.alternateIconName) ?? 0
                            
                            if index != value {
                                UIApplication.shared.setAlternateIconName(self.appIconNames.iconNames[value]) { error in
                                    if let error = error {
                                        print(error.localizedDescription)
                                    } else {
                                        print("Success! You have change the app icon.")
                                    }
                                }
                            }
                        }
                    } // : Section 1.
                    .padding(.vertical, 3)
                    
                    // MARK: - Section 2.
                    Section(header: HStack {
                        Text("Choose the app theme")
                        
                        Image(systemName: "circle.fill")
                            .resizable()
                            .frame(width: 10, height: 10)
                            .foregroundColor(self.themes[self.theme.themeSettings].themeColor)
                    }
                    ) {
                        List {
                            ForEach(self.themes, id: \.id) { item in
                                Button(action: {
                                    self.theme.themeSettings = item.id
                                    UserDefaults.standard.set(self.theme.themeSettings, forKey: "Theme")
                                    self.isThemeChanged.toggle()
                                }) {
                                    HStack {
                                        Image(systemName: "circle.fill")
                                            .foregroundColor(item.themeColor)
                                        
                                        Text(item.themeName)
                                    } // : HStack
                                } // : Button.
                                .accentColor(Color.primary)
                            }
                        }
                    } // : Section 2.
                    .padding(.vertical, 3)
                    
                    // MARK: - Section 3.
                    Section(header: Text("Follow us on social media")) {
                        FormRowLinkView(icon: "globe",
                                        color: .pink,
                                        text: "Website",
                                        link: "https://swiftuimasterclass.com")
                        FormRowLinkView(icon: "link",
                                        color: .blue,
                                        text: "Twitter",
                                        link: "https://twitter.com/ElgatitoMontes")
                        FormRowLinkView(icon: "play.rectangle",
                                        color: .green,
                                        text: "Courses",
                                        link: "https://www.udemy.com/user/robert-petras")
                    } // : Section 3.
                    .padding(.vertical, 3)
                    
                    // MARK: - Section 4.
                    Section(header: Text("About the application")) {
                        FormRowStaticView(icon: "gear",
                                          firstText: "Applicattion",
                                          secondText: "Todo")
                        FormRowStaticView(icon: "checkmark.seal",
                                          firstText: "Compatibility",
                                          secondText: "iPhone, iPad")
                        FormRowStaticView(icon: "keyboard",
                                          firstText: "Developer",
                                          secondText: "Gastón Montes")
                        FormRowStaticView(icon: "paintbrush",
                                          firstText: "Designer",
                                          secondText: "Robert Petras")
                        FormRowStaticView(icon: "flag",
                                          firstText: "Version",
                                          secondText: "1.0.0")
                    } // : Section 4.
                } // : Form.
                .listStyle(GroupedListStyle())
                .environment(\.horizontalSizeClass, .regular)
                
                // MARK: - Footer.
                Text("Copyright © All rights reserved.\nBetter Apps ♡ Less Code.")
                    .multilineTextAlignment(.center)
                    .font(.footnote)
                    .padding(.top, 6)
                    .padding(.bottom, 8)
                    .foregroundColor(Color.secondary)
            } // : VStack.
            .navigationBarItems(trailing:
                                    Button(action: {
                self.presentationMode.wrappedValue.dismiss()
            }) {
                Image(systemName: "xmark")
            }
            )
            .navigationBarTitle("Settings", displayMode: .inline)
            .background(Color("ColorBackground").edgesIgnoringSafeArea(.all))
        } // : NavigationView.
        .accentColor(self.themes[self.theme.themeSettings].themeColor)
        .navigationViewStyle(StackNavigationViewStyle())
    }
}

// MARK: - Preview.
struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView().environmentObject(AppIconNames())
    }
}
