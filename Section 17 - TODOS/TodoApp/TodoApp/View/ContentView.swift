//
//  ContentView.swift
//  TodoApp
//
//  Created by Gastón Montes on 11/04/2022.
//

import SwiftUI
import CoreData

struct ContentView: View {
    // MARK: - Properties.
    @Environment(\.managedObjectContext) private var viewContext
    
    @EnvironmentObject private var appIconNames: AppIconNames
    
    @State private var showingAddTodoView: Bool = false
    @State private var animatingPlusButton: Bool = false
    @State private var showingSettingsView: Bool = false
    
    @FetchRequest(entity: Todo.entity(),
                  sortDescriptors: [ NSSortDescriptor(keyPath: \Todo.name, ascending: true) ])
    var todos: FetchedResults<Todo>
    
    @ObservedObject var theme = ThemeSettings.shared
    var themes: [ ThemeModel ] = themeData
    
    // MARK: - Body.
    var body: some View {
        NavigationView {
            ZStack {
                List {
                    ForEach(self.todos, id: \.self) { todo in
                        HStack {
                            Circle().frame(width: 12, height: 12, alignment: .center)
                                .foregroundColor(self.colorize(priority: todo.priority ?? "Normal"))
                            
                            Text(todo.name ?? "Unknown")
                                .fontWeight(.semibold)
                            
                            Spacer()
                            
                            Text(todo.priority ?? "Unknown")
                                .font(.footnote)
                                .foregroundColor(Color(UIColor.systemGray2))
                                .padding(3)
                                .frame(minWidth: 62)
                                .overlay(
                                    Capsule().stroke(Color(UIColor.systemGray2), lineWidth: 0.75)
                                )
                        } // : HStack.
                        .padding(.vertical, 10)
                    } // : Foreach.
                    .onDelete(perform: self.deleteTodo)
                } // : List.
                .navigationBarTitle("Todo", displayMode: .inline)
                .navigationBarItems(leading: EditButton().accentColor(themes[self.theme.themeSettings].themeColor),
                                    trailing: Button(action: {
                    self.showingSettingsView.toggle()
                }) {
                    Image(systemName: "paintbrush")
                }) // : Add button.
                .accentColor(themes[self.theme.themeSettings].themeColor)
                .sheet(isPresented: $showingSettingsView) {
                    SettingsView().environmentObject(self.appIconNames)
                }
                
                if self.todos.count == 0 {
                    EmptyListView()
                }
            } // : ZStack.
            .sheet(isPresented: $showingAddTodoView) {
                AddTodoView().environment(\.managedObjectContext, self.viewContext)
            }
            .overlay(
                ZStack {
                    Group {
                        Circle()
                            .fill(themes[self.theme.themeSettings].themeColor)
                            .opacity(0.2)
                            .scaleEffect(self.animatingPlusButton ? 1 : 0)
                            .frame(width: 68, height: 68, alignment: .center)
                        
                        Circle()
                            .fill(themes[self.theme.themeSettings].themeColor)
                            .opacity(0.15)
                            .scaleEffect(self.animatingPlusButton ? 1 : 0)
                            .frame(width: 88, height: 88, alignment: .center)
                    } // : Group.
                    .onAppear(perform: {
                        withAnimation(Animation.easeInOut(duration: 2).repeatForever(autoreverses: true)) {
                            self.animatingPlusButton.toggle()
                        }
                    })
                    
                    Button(action: {
                        self.showingAddTodoView.toggle()
                    }, label: {
                        Image(systemName: "plus.circle.fill")
                            .resizable()
                            .scaledToFit()
                            .background(Circle().fill(Color("ColorBase")))
                            .frame(width: 48, height: 48, alignment: .center)
                    }) // : Plus Button.
                    .accentColor(themes[self.theme.themeSettings].themeColor)
                } // : ZStack.
                    .padding(.bottom, 15)
                    .padding(.trailing, 15)
                , alignment: .bottomTrailing
            )
        } // : Navigation.
        .accentColor(self.themes[self.theme.themeSettings].themeColor)
        .navigationViewStyle(StackNavigationViewStyle())
    }
    
    // MARK: - Functions.
    private func deleteTodo(at offsets: IndexSet) {
        for index in offsets {
            let todo = self.todos[index]
            
            self.viewContext.delete(todo)
            
            do {
                try self.viewContext.save()
            } catch {
                print(error)
            }
        }
    }
    
    private func colorize(priority: String) -> Color {
        switch priority.lowercased() {
        case "high":
            return .pink
        case "normal":
            return .green
        case "low":
            return .blue
        default:
            return .gray
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
