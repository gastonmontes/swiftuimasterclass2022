//
//  FormRowStaticView.swift
//  TodoApp
//
//  Created by Gastón Montes on 15/04/2022.
//

import SwiftUI

struct FormRowStaticView: View {
    // MARK: - Properties.
    var icon: String
    var firstText: String
    var secondText: String
    
    // MARK: - Body.
    var body: some View {
        HStack {
            ZStack {
                RoundedRectangle(cornerRadius: 8, style: .continuous)
                    .fill(.gray)
                
                Image.init(systemName: self.icon)
                    .foregroundColor(.white)
            } // : ZStack.
            .frame(width: 36, height: 36, alignment: .center)
            
            Text(self.firstText).foregroundColor(.gray)
            
            Spacer()
            
            Text(self.secondText)
        }
    }
}

// MARK: - Preview.
struct FormRowStaticView_Previews: PreviewProvider {
    static var previews: some View {
        FormRowStaticView(icon: "gear", firstText: "Aplication", secondText: "Todo")
            .previewLayout(.fixed(width: 375, height: 60))
            .padding()
    }
}
