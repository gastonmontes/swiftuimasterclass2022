//
//  EmptyListView.swift
//  TodoApp
//
//  Created by Gastón Montes on 15/04/2022.
//

import SwiftUI

struct EmptyListView: View {
    // MARK: - Properties.
    @State private var isAnimated: Bool = false
    
    let images = [ "illustration-no1",
                   "illustration-no2",
                   "illustration-no3" ]
    
    let tips = [ "Use your time wisely.",
                 "Slow and steady wins the race.",
                 "Keep it short and sweet.",
                 "Put hard tasks first.",
                 "Reward yourself after work.",
                 "Collect tasks ahead of time.",
                 "Each night schedule for tomorrow." ]
    
    @ObservedObject var theme = ThemeSettings.shared
    var themes = themeData
    
    // MARK: - Body.
    var body: some View {
        ZStack {
            VStack(alignment: .center, spacing: 20) {
                Image("\(self.images.randomElement() ?? self.images.first!)")
                    .renderingMode(.template)
                    .resizable()
                    .scaledToFit()
                    .frame(minWidth: 256,
                           idealWidth: 280,
                           maxWidth: 360,
                           minHeight: 256,
                           idealHeight: 280,
                           maxHeight: 360,
                           alignment: .center)
                    .layoutPriority(1)
                    .foregroundColor(themes[self.theme.themeSettings].themeColor)
                
                Text("\(self.tips.randomElement() ?? self.tips.first!)")
                    .layoutPriority(0.5)
                    .font(.system(.headline, design: .rounded))
                    .foregroundColor(themes[self.theme.themeSettings].themeColor)
            } // : VStack.
            .padding(.horizontal)
            .opacity(self.isAnimated ? 1 : 0)
            .offset(y: self.isAnimated ? 0 : -50)
            .onAppear(perform: {
                withAnimation(Animation.easeInOut(duration: 1.5)) {
                    self.isAnimated.toggle()
                }
            })
        } // : ZStack.
        .frame(minWidth: 0,
               maxWidth: .infinity,
               minHeight: 0,
               maxHeight: .infinity)
        .background(Color("ColorBase"))
        .edgesIgnoringSafeArea(.all)
    }
}

// MARK: - Preview.
struct EmptyListView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            EmptyListView()
                .previewInterfaceOrientation(.portraitUpsideDown)
            EmptyListView()
        }
    }
}
