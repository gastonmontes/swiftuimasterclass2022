//
//  TodoAppApp.swift
//  TodoApp
//
//  Created by Gastón Montes on 11/04/2022.
//

import SwiftUI

@main
struct TodoAppApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .environmentObject(AppIconNames())
        }
    }
}
