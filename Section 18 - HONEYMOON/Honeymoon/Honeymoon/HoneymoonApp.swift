//
//  HoneymoonApp.swift
//  Honeymoon
//
//  Created by Gastón Montes on 27/12/2022.
//

import SwiftUI

@main
struct HoneymoonApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
