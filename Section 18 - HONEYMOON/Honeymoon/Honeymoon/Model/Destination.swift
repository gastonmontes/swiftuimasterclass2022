//
//  Destination.swift
//  Honeymoon
//
//  Created by Gastón Montes on 27/12/2022.
//

import Foundation

struct Destination {
    var place: String
    var country: String
    var imageName: String
}
