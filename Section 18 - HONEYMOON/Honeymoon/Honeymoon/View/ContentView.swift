//
//  ContentView.swift
//  Honeymoon
//
//  Created by Gastón Montes on 27/12/2022.
//

import SwiftUI

struct ContentView: View {
    // MARK: - Properties.
    @State var showAlert: Bool = false
    @State private var lastCardIndex: Int = 1
    @State private var cardRemovalTransition = AnyTransition.trailingBottom
    @GestureState private var dragState = DragState.inactive
    private let dragAreaThreshold: CGFloat = 65.0
    
    @State var cardViews: [ CardView ] = {
        var views = [ CardView ]()
        
        for destination in destinationStaticData {
            views.append(CardView(honeymoon: destination))
        }
        
        return views
    }()
    
    // MARK: - Move the cards.
    private func moveCards() {
        cardViews.removeFirst()
        
        self.lastCardIndex += 1
        
        let destination = destinationStaticData[lastCardIndex % destinationStaticData.count]
        let newCardView = CardView(honeymoon: destination)
        
        cardViews.append(newCardView)
    }
    
    // MARK: - Aux functions.
    private func isTopCard(cardView: CardView) -> Bool {
        guard let index = self.cardViews.firstIndex(where: { $0.id == cardView.id }) else {
            return false
        }
        
        return index == 0
    }
    
    // MARK: - Body
    var body: some View {
        VStack {
            // MARK: - Header.
            HeaderView()
                .opacity(self.dragState.isDragging ? 0.0 : 1.0)
                .animation(.default, value: 0)
            
            Spacer()
            
            // MARK: - Cards.
            ZStack {
                ForEach(self.cardViews) { cardView in
                    cardView
                        .zIndex(self.isTopCard(cardView: cardView) ? 1 : 0)
                        .overlay(
                            ZStack {
                                Image(systemName: "x.circle")
                                    .modifier(ImageSymbolModifier())
                                    .opacity(self.dragState.translation.width < -self.dragAreaThreshold && self.isTopCard(cardView: cardView) ? 1.0 : 0.0)
                                
                                Image(systemName: "heart.circle")
                                    .modifier(ImageSymbolModifier())
                                    .opacity(self.dragState.translation.width > self.dragAreaThreshold && self.isTopCard(cardView: cardView) ? 1.0 : 0.0)
                            }
                        )
                        .offset(x: self.isTopCard(cardView: cardView) ? self.dragState.translation.width : 0,
                                y: self.isTopCard(cardView: cardView) ? self.dragState.translation.height : 0)
                        .scaleEffect(self.dragState.isDragging && self.isTopCard(cardView: cardView) ? 0.85 : 1.0)
                        .rotationEffect(Angle(degrees: self.isTopCard(cardView: cardView) ? self.dragState.translation.width / 12 : 0))
                        .animation(.interpolatingSpring(stiffness: 120, damping: 120))
                        .gesture(LongPressGesture(minimumDuration: 0.01)
                            .sequenced(before: DragGesture())
                            .updating(self.$dragState, body: { (value, state, transaction) in
                                switch value {
                                case .first(true):
                                    state = .pressing
                                case .second(true, let drag):
                                    state = .dragging(translation: drag?.translation ?? .zero)
                                default:
                                    break
                                }
                            })
                                .onChanged({ value in
                                    guard case .second(true, let drag?) = value else {
                                        return
                                    }
                                    
                                    if drag.translation.width < -self.dragAreaThreshold {
                                        self.cardRemovalTransition = .leadingBottom
                                    }
                                    
                                    if drag.translation.width > self.dragAreaThreshold {
                                        self.cardRemovalTransition = .trailingBottom
                                    }
                                })
                                .onEnded({ (value) in
                                    guard case .second(true, let drag?) = value else {
                                        return
                                    }
                                    
                                    if drag.translation.width < -self.dragAreaThreshold || drag.translation.width > self.dragAreaThreshold {
                                        playSound(sound: "sound-rise", type: "mp3")
                                        self.moveCards()
                                    }
                                })
                        ).transition(self.cardRemovalTransition)
                }
            }
            .padding(.horizontal)
            
            Spacer()
            
            // MARK: - Footer.
            FooterView(showBookingAlert: self.$showAlert)
                .opacity(self.dragState.isDragging ? 0.0 : 1.0)
                .animation(.default, value: 0)
        }
        .alert(isPresented: self.$showAlert) {
            Alert(title: Text("SUCCESS"),
                  message: Text("Whising a lovely and most precious of the times together for the amazing couple."),
                  dismissButton: .default(Text("Happy Honeymoon")))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
