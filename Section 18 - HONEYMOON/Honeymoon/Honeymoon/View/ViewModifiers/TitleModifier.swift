//
//  TitleModifier.swift
//  Honeymoon
//
//  Created by Gastón Montes on 27/12/2022.
//

import SwiftUI

struct TitleModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.largeTitle)
            .foregroundColor(.pink)
    }
}
