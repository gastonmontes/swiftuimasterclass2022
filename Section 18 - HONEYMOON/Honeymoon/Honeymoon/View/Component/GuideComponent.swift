//
//  GuideComponent.swift
//  Honeymoon
//
//  Created by Gastón Montes on 27/12/2022.
//

import SwiftUI

struct GuideComponent: View {
    // MARK: - Properties.
    var title: String
    var subtitle: String
    var description: String
    var icon: String
    
    var body: some View {
        HStack(alignment: .center, spacing: 20) {
            Image(systemName: self.icon)
                .font(.largeTitle)
                .foregroundColor(.pink)
            
            VStack(alignment: .leading, spacing: 4) {
                HStack {
                    Text(self.title.uppercased())
                        .font(.title)
                        .fontWeight(.heavy)
                    
                    Spacer()
                    
                    Text(self.subtitle.uppercased())
                        .font(.footnote)
                        .fontWeight(.heavy)
                        .foregroundColor(.pink)
                }
                
                Divider().padding(.bottom, 4)
                
                Text(self.description)
                    .font(.footnote)
                    .foregroundColor(.secondary)
                    .fixedSize(horizontal: false, vertical: true)
            }
        }
    }
}

struct GuideComponent_Previews: PreviewProvider {
    static var previews: some View {
        GuideComponent(title: "Title",
                       subtitle: "Swipe Right",
                       description: "This is a placeholder sentence. This is a placeholder sentence. This is a placeholder sentence.",
                       icon: "heart.circle")
            .previewLayout(.sizeThatFits)
    }
}
