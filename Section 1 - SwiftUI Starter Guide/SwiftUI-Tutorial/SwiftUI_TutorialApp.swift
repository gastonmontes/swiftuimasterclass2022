//
//  SwiftUI_TutorialApp.swift
//  SwiftUI-Tutorial
//
//  Created by Gastón Montes on 15/02/2022.
//

import SwiftUI

@main
struct SwiftUI_TutorialApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
