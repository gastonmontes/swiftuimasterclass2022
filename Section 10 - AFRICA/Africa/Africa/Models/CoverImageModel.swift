//
//  CoverImageModel.swift
//  Africa
//
//  Created by Gastón Montes on 08/09/2022.
//

import Foundation

struct CoverImageModel: Identifiable, Codable {
    let id: Int
    let name: String
}
