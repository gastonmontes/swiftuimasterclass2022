//
//  VideoModel.swift
//  Africa
//
//  Created by Gastón Montes on 11/01/2023.
//

import Foundation

struct VideoModel: Codable, Identifiable {
    let id: String
    let name: String
    let headline: String
    
    var thumbnail: String {
        "video-\(self.id)"
    }
}
