//
//  MapLocationModel.swift
//  Africa
//
//  Created by Gastón Montes on 11/01/2023.
//

import Foundation

struct MapLocationModel: Codable, Identifiable {
    let id: String
    let name: String
    let image: String
    let latitude: Double
    let longitude: Double
}
