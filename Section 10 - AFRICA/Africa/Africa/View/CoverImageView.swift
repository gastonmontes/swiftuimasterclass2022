//
//  CoverImageView.swift
//  Africa
//
//  Created by Gastón Montes on 08/09/2022.
//

import SwiftUI

struct CoverImageView: View {
    // MARK: - Properties.
    let coverImages: [ CoverImageModel ] = Bundle.main.decode(file: "covers.json")
    
    // MARK: - Body.
    var body: some View {
        TabView {
            ForEach(self.coverImages) { item in
                Image(item.name)
                    .resizable()
                    .scaledToFill()
            } // : ForEach.
        } // : Tab.
        .tabViewStyle(PageTabViewStyle())
    }
}

// MARK: - Preview.
struct CoverImageView_Previews: PreviewProvider {
    static var previews: some View {
        CoverImageView().previewLayout(.fixed(width: 400, height: 300))
    }
}
