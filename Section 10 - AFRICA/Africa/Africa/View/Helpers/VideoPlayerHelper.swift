//
//  VideoPlayerHelper.swift
//  Africa
//
//  Created by Gastón Montes on 11/01/2023.
//

import Foundation
import AVKit

var videoPlayer: AVPlayer?

func playVideo(fileName: String, fileFormat: String) -> AVPlayer {
    guard let videoURL = Bundle.main.url(forResource: fileName, withExtension: fileFormat) else {
        return AVPlayer()
    }
    
    videoPlayer = AVPlayer(url: videoURL)
    videoPlayer!.play()
    
    return videoPlayer!
}
