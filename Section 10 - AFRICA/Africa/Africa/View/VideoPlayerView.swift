//
//  VideoPlayerView.swift
//  Africa
//
//  Created by Gastón Montes on 11/01/2023.
//

import SwiftUI
import AVKit

struct VideoPlayerView: View {
    // MARK: - Properties.
    var videoSelected: String
    var videoTitle: String
    
    // MARK: - Body.
    var body: some View {
        VStack {
            VideoPlayer(player: playVideo(fileName: self.videoSelected,
                                          fileFormat: "mp4"))
            .overlay(
                Image("logo")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 32, height: 32)
                    .padding(.top, 6)
                    .padding(.horizontal, 8)
                , alignment: .topLeading)
        } // : VStack.
        .tint(.accentColor)
        .navigationTitle(self.videoTitle)
        .navigationBarTitleDisplayMode(.inline)
    }
}

// MARK: - Properties.
struct VideoPlayerView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            VideoPlayerView(videoSelected: "lion", videoTitle: "Lion")
        }
    }
}
