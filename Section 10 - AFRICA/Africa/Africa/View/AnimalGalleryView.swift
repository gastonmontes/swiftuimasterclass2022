//
//  AnimalGalleryView.swift
//  Africa
//
//  Created by Gastón Montes on 05/01/2023.
//

import SwiftUI

struct AnimalGalleryView: View {
    // MARK: - Properties.
    let animal: AnimalModel
    
    // MARK: - Body.
    var body: some View {
        ScrollView(.horizontal, showsIndicators: false) {
            HStack(alignment: .center, spacing: 16) {
                ForEach(animal.gallery, id: \.self) { item in
                    Image(item)
                        .resizable()
                        .scaledToFit()
                        .frame(height: 200)
                    .cornerRadius(12)
                } // : ForEach
            } // : HStack.
        } // : ScrollView.
    }
}

struct AnimalGalleryView_Previews: PreviewProvider {
    static let animals: [ AnimalModel ] = Bundle.main.decode(file: "animals.json")
    
    static var previews: some View {
        AnimalGalleryView(animal: animals[4])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
