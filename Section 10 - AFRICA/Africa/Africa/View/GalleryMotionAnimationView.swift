//
//  GalleryMotionAnimationView.swift
//  Africa
//
//  Created by Gastón Montes on 11/01/2023.
//

import SwiftUI

struct GalleryMotionAnimationView: View {
    // MARK: - Properties.
    @State private var randomCircle = Int.random(in: 12...16)
    @State private var isAnimation = false
    
    // MARK: - Functions.
    func randomCoordinate(max: CGFloat) -> CGFloat {
        return CGFloat.random(in: 0...max)
    }
    
    func randomSize() -> CGFloat {
        return CGFloat(Int.random(in: 10...300))
    }
    
    func randomScale() -> CGFloat {
        return CGFloat(Double.random(in: 0.1...2.0))
    }
    
    func randomSpeed() -> Double {
        return Double.random(in: 0.025...1.0)
    }
    
    func randomDelay() -> Double {
        return Double.random(in: 0...2)
    }
    
    // MARK: - Body.
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                ForEach(0...self.randomCircle, id: \.self) { item in
                    Circle()
                        .foregroundColor(.gray)
                        .opacity(0.15)
                        .frame(width: self.randomSize(),
                               height: self.randomSize(),
                               alignment: .center)
                        .scaleEffect(self.isAnimation ? self.randomScale() : 1)
                        .position(x: self.randomCoordinate(max: geometry.size.width),
                                  y: self.randomCoordinate(max: geometry.size.height))
                        .animation(
                            Animation.interpolatingSpring(stiffness: 0.5, damping: 0.5)
                                .repeatForever()
                                .speed(self.randomSpeed())
                                .delay(self.randomDelay())
                        )
                        .onAppear {
                            self.isAnimation = true
                        }
                }
            } // : ZStack.
            .drawingGroup()
        } // : Geometry
    }
}

// MARK: - Preview.
struct GalleryMotionAnimationView_Previews: PreviewProvider {
    static var previews: some View {
        GalleryMotionAnimationView()
    }
}
