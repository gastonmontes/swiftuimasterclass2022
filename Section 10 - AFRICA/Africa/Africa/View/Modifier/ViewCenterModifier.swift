//
//  ViewCenterModifier.swift
//  Africa
//
//  Created by Gastón Montes on 17/01/2023.
//

import SwiftUI

struct ViewCenterModifier: ViewModifier {
    func body(content: Content) -> some View {
        HStack {
            Spacer()
            
            content
            
            Spacer()
        }
    }
}
