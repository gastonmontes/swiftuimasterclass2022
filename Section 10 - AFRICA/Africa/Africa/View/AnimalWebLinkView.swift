//
//  AnimalWebLinkView.swift
//  Africa
//
//  Created by Gastón Montes on 05/01/2023.
//

import SwiftUI

struct AnimalWebLinkView: View {
    // MARK: - Properties.
    let animal: AnimalModel
    
    // MARK: - Body.
    var body: some View {
        GroupBox {
            HStack {
                Image(systemName: "globe")
                
                Text("Wikipedia")
                
                Spacer()
                
                Group {
                    Image(systemName: "arrow.up.right.square")
                    
                    Link(animal.name,
                         destination: URL(string: animal.link) ?? URL(string: "http://wikipedia.org")!)
                }
                .foregroundColor(.accentColor)
            } // : HStack.
        } // : Box.
    }
}

struct AnimalWebLinkView_Previews: PreviewProvider {
    static let animals: [ AnimalModel ] = Bundle.main.decode(file: "animals.json")
    
    static var previews: some View {
        AnimalWebLinkView(animal: animals[3])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
