//
//  AnimalFactsView.swift
//  Africa
//
//  Created by Gastón Montes on 05/01/2023.
//

import SwiftUI

struct AnimalFactsView: View {
    // MARK: - Properties.
    let animal: AnimalModel
    
    // MARK: - Body.
    var body: some View {
        GroupBox {
            TabView {
                ForEach(self.animal.fact, id: \.self) { item in
                    Text(item)
                }
            } // : Tabs.
            .tabViewStyle(PageTabViewStyle())
            .frame(minHeight: 148, idealHeight: 168, maxHeight: 180)
        } // : Box.
    }
}

struct AnimalFactsView_Previews: PreviewProvider {
    static let animals: [ AnimalModel ] = Bundle.main.decode(file: "animals.json")
    
    static var previews: some View {
        AnimalFactsView(animal: animals[5])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
