//
//  MapAnnotationView.swift
//  Africa
//
//  Created by Gastón Montes on 11/01/2023.
//

import SwiftUI

struct MapAnnotationView: View {
    // MARK: - Properties.
    var location: MapLocationModel
    
    @State private var animation: Double = 0.0
    
    // MARK: - Body.
    var body: some View {
        ZStack {
            Circle()
                .fill(Color.accentColor)
                .frame(width: 54, height: 54, alignment: .center)
            
            Circle()
                .stroke(Color.accentColor, lineWidth: 2)
                .frame(width: 52, height: 52, alignment: .center)
                .scaleEffect(1 + CGFloat(self.animation))
                .opacity(1 - self.animation)
            
            Image(self.location.image)
                .resizable()
                .scaledToFit()
                .frame(width: 48, height: 48, alignment: .center)
            .clipShape(Circle())
        } // : ZStack.
        .onAppear {
            withAnimation(Animation.easeOut(duration: 2).repeatForever(autoreverses: false)) {
                animation = 1
            }
        }
    }
}

// MARK: - Preview.
struct MapAnnotationView_Previews: PreviewProvider {
    static var locations: [ MapLocationModel ] = Bundle.main.decode(file: "locations.json")
    
    static var previews: some View {
        MapAnnotationView(location: locations[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
