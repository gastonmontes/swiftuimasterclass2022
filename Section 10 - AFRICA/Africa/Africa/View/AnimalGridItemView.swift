//
//  AnimalGridItemView.swift
//  Africa
//
//  Created by Gastón Montes on 17/01/2023.
//

import SwiftUI

struct AnimalGridItemView: View {
    // MARK: - Properties.
    let animal: AnimalModel
    
    // MARK: - Body.
    var body: some View {
        Image(self.animal.image)
            .resizable()
            .scaledToFit()
            .cornerRadius(12)
    }
}

// MARK: - Preview.
struct AnimalGridItemView_Previews: PreviewProvider {
    static let animalModels:  [ AnimalModel ] = Bundle.main.decode(file: "animals.json")
    
    static var previews: some View {
        AnimalGridItemView(animal: animalModels[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
