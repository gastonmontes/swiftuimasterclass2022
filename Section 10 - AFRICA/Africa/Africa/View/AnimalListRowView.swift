//
//  AnimalListRowView.swift
//  Africa
//
//  Created by Gastón Montes on 14/09/2022.
//

import SwiftUI

struct AnimalListRowView: View {
    // MARK: - Properties.
    let animal: AnimalModel
    
    // MARK: - Body.
    var body: some View {
        HStack(alignment: .center, spacing: 16) {
            Image(self.animal.image)
                .resizable()
                .scaledToFill()
                .frame(width: 90, height: 90)
                .clipShape(RoundedRectangle(cornerRadius: 12))
            
            VStack(alignment: .leading, spacing: 8) {
                Text(self.animal.name)
                    .font(.title2)
                    .fontWeight(.heavy)
                    .foregroundColor(.accentColor)
                
                Text(self.animal.headline)
                    .font(.footnote)
                    .multilineTextAlignment(.leading)
                    .lineLimit(2)
                    .padding(.trailing, 8)
            }
        } // : HStack.
    }
}

// MARK: - Preview.
struct AnimalListRowView_Previews: PreviewProvider {
    static let animals: [ AnimalModel ] = Bundle.main.decode(file: "animals.json")
    
    static var previews: some View {
        AnimalListRowView(animal: animals[1])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
