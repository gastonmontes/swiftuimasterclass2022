//
//  VideoListItemView.swift
//  Africa
//
//  Created by Gastón Montes on 11/01/2023.
//

import SwiftUI

struct VideoListItemView: View {
    // MARK: - Properties.
    let video: VideoModel
    
    // MARK: - Body.
    var body: some View {
        HStack(spacing: 10) {
            ZStack {
                Image(self.video.thumbnail)
                    .resizable()
                    .scaledToFit()
                    .frame(height: 80)
                    .cornerRadius(9)
                
                Image(systemName: "play.circle")
                    .resizable()
                    .scaledToFit()
                    .frame(height: 32)
                    .shadow(radius: 4)
            } // : ZStack.
            
            VStack(alignment: .leading, spacing: 10) {
                Text(self.video.name)
                    .font(.title2)
                    .fontWeight(.heavy)
                    .foregroundColor(.accentColor)
                
                Text(self.video.headline)
                    .font(.footnote)
                    .multilineTextAlignment(.leading)
                    .lineLimit(2)
            } // : VStack.
        } // : HStack.
    }
}

// MARK: - Preview.
struct VideoListItem_Previews: PreviewProvider {
    static let videos: [ VideoModel ] = Bundle.main.decode(file: "videos.json")
    
    static var previews: some View {
        VideoListItemView(video: videos[0])
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
