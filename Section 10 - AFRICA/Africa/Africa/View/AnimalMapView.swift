//
//  AnimalMapView.swift
//  Africa
//
//  Created by Gastón Montes on 05/01/2023.
//

import SwiftUI
import MapKit

struct AnimalMapView: View {
    // MARK: - Properties.
    @State private var region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 6.600286,
                                                                                  longitude: 16.4377599),
                                                   span: MKCoordinateSpan(latitudeDelta: 60.0,
                                                                          longitudeDelta: 60.0))
    
    // MARK: - Body.
    var body: some View {
        Map(coordinateRegion: self.$region)
            .overlay(
                NavigationLink(destination: MapView()) {
                    HStack {
                        Image(systemName: "mappin.circle")
                            .foregroundColor(.white)
                            .imageScale(.large)
                        
                        Text("Locations")
                            .foregroundColor(.accentColor)
                            .fontWeight(.bold)
                    } // : HStack.
                    .padding(.vertical, 10)
                    .padding(.horizontal, 40)
                    .background(
                        Color.black
                            .opacity(0.4)
                            .cornerRadius(8)
                    )
                } // : Navigation.
                    .padding(12)
                , alignment: .topTrailing
            )
            .frame(height: 256)
            .cornerRadius(12)
    }
}

struct AnimalMapView_Previews: PreviewProvider {
    static var previews: some View {
        AnimalMapView()
            .previewLayout(.sizeThatFits)
            .padding()
    }
}
