//
//  MapView.swift
//  Africa
//
//  Created by Gastón Montes on 07/09/2022.
//

import SwiftUI
import MapKit

struct MapView: View {
    // MARK: - Properties.
    @State private var region: MKCoordinateRegion = {
        var mapCoordinates = CLLocationCoordinate2D(latitude: 6.600286,
                                                    longitude: 16.4377599)
        var mapZoomLeven = MKCoordinateSpan(latitudeDelta: 70.0,
                                            longitudeDelta: 70.0)
        var mapRegion = MKCoordinateRegion(center: mapCoordinates,
                                           span: mapZoomLeven)
        
        return mapRegion
    }()
    
    let mapLocations: [ MapLocationModel ] = Bundle.main.decode(file: "locations.json")
    
    // MARK: - Body.
    var body: some View {
        Map(coordinateRegion: self.$region,
            annotationItems: self.mapLocations) { item in
            MapAnnotation(coordinate: CLLocationCoordinate2D(latitude: item.latitude,
                                                             longitude: item.longitude)) {
                MapAnnotationView(location: item)
            }
        } // : Map.
            .overlay(
                HStack(alignment: .center, spacing: 12) {
                    Image("compass")
                        .resizable()
                        .scaledToFit()
                        .frame(width: 48, height: 48)
                    
                    VStack(alignment: .leading) {
                        HStack {
                            Text("Latitude: ")
                                .font(.footnote)
                                .fontWeight(.bold)
                                .foregroundColor(.accentColor)
                            
                            Spacer()
                            
                            Text("\(self.region.center.latitude)")
                                .font(.footnote)
                                .foregroundColor(.white)
                        }
                        
                        Divider()
                        
                        HStack {
                            Text("Longitude: ")
                                .font(.footnote)
                                .fontWeight(.bold)
                                .foregroundColor(.accentColor)
                            
                            Spacer()
                            
                            Text("\(self.region.center.longitude)")
                                .font(.footnote)
                                .foregroundColor(.white)
                        }
                    }
                } // : HStack.
                    .padding(.vertical, 12)
                    .padding(.horizontal, 16)
                    .background(
                        Color.black
                            .cornerRadius(8)
                            .opacity(0.6)
                    )
                    .padding()
                , alignment: .top
            )
    }
}

// MARK: - Preview.
struct MapView_Previews: PreviewProvider {
    static var previews: some View {
        MapView()
    }
}
