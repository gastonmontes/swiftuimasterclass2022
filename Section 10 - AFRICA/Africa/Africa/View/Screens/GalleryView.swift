//
//  GalleryView.swift
//  Africa
//
//  Created by Gastón Montes on 07/09/2022.
//

import SwiftUI

struct GalleryView: View {
    // MARK: - Properties.
    let animals: [ AnimalModel ] = Bundle.main.decode(file: "animals.json")
    let haptic = UIImpactFeedbackGenerator(style: .medium)
    
    @State private var selectedAnimal = "lion"
    @State private var gridLayout: [ GridItem ] = [ GridItem(.flexible()) ]
    @State private var gridColumn: Double = 3.0
    
    // MARK: - Body.
    func gridSwitch() {
        self.gridLayout = Array(repeating: GridItem(.flexible()),
                                count: Int(self.gridColumn))
    }
    
    // MARK: - Body.
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            VStack(alignment: .center, spacing: 30) {
                Image(self.selectedAnimal)
                    .resizable()
                    .scaledToFit()
                    .clipShape(Circle())
                    .overlay(Circle().stroke(.white, lineWidth: 8))
                
                Slider(value: self.$gridColumn, in: 2 ... 4, step: 1)
                    .padding(.horizontal)
                    .onChange(of: self.gridColumn) { newValue in
                        self.gridSwitch()
                    }
                
                LazyVGrid(columns: self.gridLayout,
                          alignment: .center,
                          spacing: 10) {
                    ForEach(self.animals) { item in
                        Image(item.image)
                            .resizable()
                            .scaledToFit()
                            .clipShape(Circle())
                            .overlay(Circle().stroke(.white, lineWidth: 1))
                            .onTapGesture {
                                self.haptic.impactOccurred()
                                self.selectedAnimal = item.image
                            }
                    } // : For.
                } // : VGrid.
                          .animation(.easeIn)
                          .onAppear {
                              self.gridSwitch()
                          }
            } // : VStack.
            .padding(.horizontal, 10)
            .padding(.vertical, 50)
        } // : Scrool.
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .background(
            GalleryMotionAnimationView()
        )
    }
}

// MARK: - Previews.
struct GaleryView_Previews: PreviewProvider {
    static var previews: some View {
        GalleryView()
    }
}
