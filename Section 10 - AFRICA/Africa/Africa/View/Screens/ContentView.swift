//
//  ContentView.swift
//  Africa
//
//  Created by Gastón Montes on 07/09/2022.
//

import SwiftUI

struct ContentView: View {
    // MARK: - Properties.
    let animals: [ AnimalModel ] = Bundle.main.decode(file: "animals.json")
    let haptics = UIImpactFeedbackGenerator(style: .medium)
    
    @State private var isGridViewActive: Bool = false
    @State private var gridLayout: [ GridItem ] = [ GridItem(.flexible()) ]
    @State private var gridColumns: Int = 1
    @State private var toolbarIcon = "square.grid.2x2"
    
    // MARK: - functions.
    private func gridSwitch() {
        self.gridLayout = Array(repeating: GridItem(.flexible()),
                                count: self.gridLayout.count % 3 + 1)
        self.gridColumns = self.gridLayout.count
        
        switch self.gridColumns {
        case 1:
            self.toolbarIcon = "square.grid.2x2"
        case 2:
            self.toolbarIcon = "square.grid.3x2"
        case 3:
            self.toolbarIcon = "rectangle.grid.1x2"
        default:
            self.toolbarIcon = "square.grid.2x2"
        }
    }
    
    // MARK: - Body.
    var body: some View {
        NavigationView {
            Group {
                if !self.isGridViewActive {
                    List() {
                        CoverImageView()
                            .frame(height: 300)
                            .listRowInsets(EdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0))
                        
                        ForEach(animals) { animal in
                            NavigationLink(destination: AnimalDetailView(animal: animal)) {
                                AnimalListRowView(animal: animal)
                            } // : Link.
                        } // : Loop.
                        
                        CreditsView()
                            .modifier(ViewCenterModifier())
                    } // : List.
                } else {
                    ScrollView(.vertical, showsIndicators: false) {
                        LazyVGrid(columns: self.gridLayout, alignment: .center, spacing: 10) {
                            ForEach(self.animals) { animal in
                                NavigationLink(destination: AnimalDetailView(animal: animal)) {
                                    AnimalGridItemView(animal: animal)
                                } // : Link.
                            } // Loop.
                        } // : VGrid.
                        .padding(10)
                        .animation(.easeIn)
                    } // : Scroll.
                } // : Condition.
            } // : Group.
            .navigationTitle("Africa")
            .navigationBarTitleDisplayMode(.large)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    HStack(spacing: 16) {
                        Button {
                            self.isGridViewActive = false
                            self.haptics.impactOccurred()
                        } label: {
                            Image(systemName: "square.fill.text.grid.1x2")
                                .font(.title2)
                                .foregroundColor(self.isGridViewActive ? .primary : .accentColor)
                        }
                        
                        Button {
                            self.isGridViewActive = true
                            self.haptics.impactOccurred()
                            self.gridSwitch()
                        } label: {
                            Image(systemName: self.toolbarIcon)
                                .font(.title2)
                                .foregroundColor(self.isGridViewActive ? .accentColor : .primary)
                        }
                    } // :  HStack.
                } // : Tool bar buttons.
            }
        } // : Navigation.
    }
}

// MARK: - Preview.
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
