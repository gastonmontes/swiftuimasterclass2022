//
//  VideoListView.swift
//  Africa
//
//  Created by Gastón Montes on 07/09/2022.
//

import SwiftUI

struct VideoListView: View {
    // MARK: - Properties.
    @State var videos: [ VideoModel ] = Bundle.main.decode(file: "videos.json")
    let hapticImpact = UIImpactFeedbackGenerator(style: .medium)
    
    // MARK: - Body.
    var body: some View {
        NavigationView {
            List(self.videos) { video in
                NavigationLink(destination: VideoPlayerView(videoSelected: video.id,
                                                            videoTitle: video.name)) {
                    VideoListItemView(video: video)
                        .padding(.vertical, 8)
                }
            } // : List.
            .listStyle(InsetGroupedListStyle())
            .navigationTitle("Videos")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar {
                ToolbarItem(placement: .navigationBarTrailing) {
                    Button {
                        self.videos.shuffle()
                        
                        self.hapticImpact.impactOccurred()
                    } label: {
                        Image(systemName: "arrow.2.squarepath")
                    }
                }
            }
        } // : NavigationView
    }
}

// MARK: - Preview.
struct VideoListView_Previews: PreviewProvider {
    static var previews: some View {
        VideoListView()
    }
}
