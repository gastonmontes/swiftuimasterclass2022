//
//  AfricaApp.swift
//  Africa
//
//  Created by Gastón Montes on 07/09/2022.
//

import SwiftUI

@main
struct AfricaApp: App {
    var body: some Scene {
        WindowGroup {
            MainView()
        }
    }
}
